package me.bagus.foodcombining;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ModePickerActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_mode_picker);
    ButterKnife.bind(this);
  }

  @OnClick(R.id.admin_mode) public void onAdminMode(View view) {
    Intent intent = new Intent(this, AdminModeActivity.class);
    startActivity(intent);
  }

  @OnClick(R.id.member_mode) public void onMemberMode(View view) {
    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
  }
}
