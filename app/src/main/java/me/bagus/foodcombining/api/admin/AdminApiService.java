package me.bagus.foodcombining.api.admin;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AdminApiService {
    public static AdminApi.AturanDiagnosis getDiagnosisApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.AturanDiagnosis.class);
    }

    public static AdminApi.GejalaApi getGejalaApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.GejalaApi.class);
    }

    public static AdminApi.MemberApi getMemberApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.MemberApi.class);
    }

    public static AdminApi.RecipeApi getRecipeApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.RecipeApi.class);
    }

    public static AdminApi.KonsultasiApi getKonsultasiApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.KonsultasiApi.class);
    }

    public static AdminApi.VideoApi getVideoApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.VideoApi.class);
    }

    public static AdminApi.FaqApi getFaqApiServices() {
        return new Retrofit.Builder().baseUrl(AdminApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AdminApi.FaqApi.class);
    }
}
