package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class AdminMember {
  public int id;
  public String nama;
  public String email;
}
