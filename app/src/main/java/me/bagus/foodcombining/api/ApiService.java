package me.bagus.foodcombining.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {

  public static ApiInterface.PostInterface getPostServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.PostInterface.class);
  }

  public static ApiInterface.SwipeGameInterface getSwipeGameServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.SwipeGameInterface.class);
  }

  public static ApiInterface.FaqInterface getFaqServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.FaqInterface.class);
  }

  public static ApiInterface.FoodDiaryInterface getDiaryServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.FoodDiaryInterface.class);
  }

  public static ApiInterface.QuizInterface getQuizServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.QuizInterface.class);
  }

  public static ApiInterface.UserInterface getUserServices() {
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient.Builder client = new OkHttpClient.Builder();

    client.addNetworkInterceptor(loggingInterceptor);

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.UserInterface.class);
  }

  public static ApiInterface.KonsultasiInterface getKonsultasiServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.KonsultasiInterface.class);
  }

  public static ApiInterface.FoodMenuInterface getFoodMenuServices() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.FoodMenuInterface.class);
  }

  public static ApiInterface.GejalaInterface getDiagnosisInteface() {
    OkHttpClient client = new OkHttpClient();

    return new Retrofit.Builder().baseUrl(ApiInterface.SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiInterface.GejalaInterface.class);
  }
}
