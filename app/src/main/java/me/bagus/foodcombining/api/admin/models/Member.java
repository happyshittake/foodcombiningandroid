package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Member {
  public int id;
  public String nama;
  public String email;
  public boolean admin;
}
