package me.bagus.foodcombining.api.models;

public class MenuRecipe {
  public int id;
  public String nama;
  public String keterangan;
  public String deskripsi;
  public String author;
  public String image;
}
