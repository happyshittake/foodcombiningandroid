package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Gejala {
  public int id;
  public String nama;
}
