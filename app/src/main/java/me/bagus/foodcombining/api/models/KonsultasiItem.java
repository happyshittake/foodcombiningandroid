package me.bagus.foodcombining.api.models;

/**
 * Created by ndjoe on 22/06/16.
 */
public class KonsultasiItem {
  public int id;
  public String pertanyaan;
  public String jawaban;
  public String tanggal;
  public String author;
}
