package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Video {
  public int id;
  public String url;
}
