package me.bagus.foodcombining.api.models;


/**
 * Created by ndjoe on 22/06/16.
 */
public class MenuComment {
    public int id;
    public String content;
    public String created_at;
    public RegisteredUser user;

    @Override
    public String toString() {
        return "MenuComment{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", created_at='" + created_at + '\'' +
                ", user=" + user +
                '}';
    }
}
