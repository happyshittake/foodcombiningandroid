package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Penyebab {
  public int id;
  public String nama;
  public String solusi;
}
