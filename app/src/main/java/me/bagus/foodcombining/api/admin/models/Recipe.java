package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Recipe {
  public int id;
  public String nama;
  public String kategori;
  public String deskripsi;
  public String keterangan;
  public String author;
  public int fixed;
  public String image;
}
