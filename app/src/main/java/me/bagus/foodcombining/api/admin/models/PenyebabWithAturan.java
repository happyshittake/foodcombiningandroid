package me.bagus.foodcombining.api.admin.models;

import java.util.ArrayList;
import org.parceler.Parcel;

@Parcel public class PenyebabWithAturan {
  public int id;
  public String nama;
  public ArrayList<Gejala> gejalas;
}
