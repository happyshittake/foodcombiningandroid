package me.bagus.foodcombining.api;

import java.util.ArrayList;

import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.EventDiary;
import me.bagus.foodcombining.api.models.Faq;
import me.bagus.foodcombining.api.models.Gejala;
import me.bagus.foodcombining.api.models.KonsultasiItem;
import me.bagus.foodcombining.api.models.MenuRecipe;
import me.bagus.foodcombining.api.models.MenuRecipeDetail;
import me.bagus.foodcombining.api.models.Post;
import me.bagus.foodcombining.api.models.Quiz;
import me.bagus.foodcombining.api.models.QuizScore;
import me.bagus.foodcombining.api.models.Register;
import me.bagus.foodcombining.api.models.Response;
import me.bagus.foodcombining.api.models.ResultDiagnosis;
import me.bagus.foodcombining.api.models.SwipeCard;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiInterface {
    public static String SERVER_BASE_URL = "http://food-combining.rhcloud.com/";

    public interface GejalaInterface {
        @GET("api/gejala")
        Call<Base<ArrayList<Gejala>>> getGejalas();

        @GET("api/diagnosis")
        Call<ResultDiagnosis> getDiagnosis(@Query("gejala") String gejala);
    }

    public interface PostInterface {
        @GET("api/post")
        Call<Base<ArrayList<Post>>> getPosts(@Query("s") String search);
    }

    public interface SwipeGameInterface {
        @GET("api/game")
        Call<Base<ArrayList<SwipeCard>>> getCards(@Query("time") String time);

        @GET("api/game/scores")
        Call<Base<ArrayList<QuizScore>>> getAllScores();

        @FormUrlEncoded
        @POST("api/game/scores")
        Call<AfterChangeCallback> saveScore(@Field("user_id") int userId, @Field("score") int score);

    }

    public interface FaqInterface {
        @GET("api/faq")
        Call<Base<ArrayList<Faq>>> getFaqs();
    }

    public interface FoodDiaryInterface {
        @GET("api/diary")
        Call<Base<ArrayList<EventDiary>>> getEvents(@Query("time") String time,
                                                    @Query("user") int id);

        @GET("api/diary/day")
        Call<Base<ArrayList<EventDiary>>> getEventsByDay(
                @Query("time") String time, @Query("user") int id);

        @FormUrlEncoded
        @POST("/api/diary")
        Call<Response> storeDiary(@Field("menu") String menu,
                                  @Field("keterangan") String kategori, @Field("waktu") String time, @Field("user") int id);
    }

    public interface QuizInterface {
        @GET("api/quiz")
        Call<Base<ArrayList<Quiz>>> getQuizzes();

        @GET("api/quiz/score")
        Call<Base<ArrayList<QuizScore>>> getAllScores();

        @FormUrlEncoded
        @POST("api/quiz/score")
        Call<Response> saveScore(@Field("user_id") int userId,
                                 @Field("score") int score);
    }

    public interface UserInterface {
        @FormUrlEncoded
        @POST("api/register")
        Call<Register> registerUser(@Field("nama") String nama,
                                    @Field("email") String email, @Field("token") String token);
    }

    public interface KonsultasiInterface {
        @GET("api/konsultasi/user")
        Call<Base<ArrayList<KonsultasiItem>>> getUserQuestions(
                @Query("user") int id);

        @FormUrlEncoded
        @POST("api/konsultasi")
        Call<Response> postQuestion(
                @Field("pertanyaan") String pertanyaan, @Field("user") int id);
    }

    public interface FoodMenuInterface {
        @GET("api/menu")
        Call<Base<ArrayList<MenuRecipe>>> getFoodMenu(@Query("user") int id);

        @FormUrlEncoded
        @POST("api/menu/my")
        Call<Base<ArrayList<MenuRecipe>>> getMyRecipes(
                @Field("userid") int id);

        @GET("api/menu/{id}")
        Call<Base<MenuRecipeDetail>> getFoodMenuDetail(@Path("id") int id);

        @GET("api/menu/{id}/delete")
        Call<Response> deleteMenu(@Path("id") int id);

        @FormUrlEncoded
        @POST("api/menu/{id}/comment")
        Call<Response> postComment(@Path("id") int id,
                                   @Field("content") String content, @Field("user") int userId);

        @Multipart
        @POST("api/menu/recipe")
        Call<Response> postRecipe(@Part("name") RequestBody name,
                                  @Part("deskripsi") RequestBody deskripsi, @Part("kategori") RequestBody kategori,
                                  @Part("keterangan") RequestBody keterangan, @Part("user") RequestBody userId,
                                  @Part MultipartBody.Part file);

        @Multipart
        @POST("api/menu/{id}/update")
        Call<Response> postUpdateRecipe(
                @Part("nama") RequestBody name, @Part("deskripsi") RequestBody deskripsi,
                @Part("kategori") RequestBody kategori, @Part("keterangan") RequestBody keterangan,
                @Part MultipartBody.Part file, @Path("id") int id);
    }
}
