package me.bagus.foodcombining.api.models;

public class ResultDiagnosis {
  public boolean found;
  public Penyebab penyebab;

  public class Penyebab {
    public int id;
    public String nama;
    public String solusi;
  }
}
