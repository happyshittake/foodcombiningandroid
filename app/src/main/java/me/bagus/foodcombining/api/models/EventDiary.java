package me.bagus.foodcombining.api.models;

public class EventDiary {
    public String menu;
    public String kategori;
    public String time;

    @Override
    public String toString() {
        return "EventDiary{" +
                "menu='" + menu + '\'' +
                ", kategori='" + kategori + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
