package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Consultation {
  public int id;
  public String pertanyaan;
  public String jawaban;
  public Tanggal tanggal;
  public String author;
}
