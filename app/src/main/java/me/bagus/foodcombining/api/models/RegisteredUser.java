package me.bagus.foodcombining.api.models;

public class RegisteredUser {
  public int id;
  public String nama;
  public String email;
  public String token;
  public String created_at;
  public String updated_at;

  @Override public String toString() {
    return "RegisteredUser{" +
        "nama='" + nama + '\'' +
        ", email='" + email + '\'' +
        ", token='" + token + '\'' +
        ", created_at='" + created_at + '\'' +
        ", updated_at='" + updated_at + '\'' +
        ", id=" + id +
        '}';
  }
}
