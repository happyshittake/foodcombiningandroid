package me.bagus.foodcombining.api.admin;

import java.util.ArrayList;

import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Consultation;
import me.bagus.foodcombining.api.admin.models.FaqCategory;
import me.bagus.foodcombining.api.admin.models.Gejala;
import me.bagus.foodcombining.api.admin.models.Member;
import me.bagus.foodcombining.api.admin.models.Penyebab;
import me.bagus.foodcombining.api.admin.models.PenyebabWithAturan;
import me.bagus.foodcombining.api.admin.models.Recipe;
import me.bagus.foodcombining.api.admin.models.Video;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class AdminApi {
    public static String BASE_URL = "http://food-combining.rhcloud.com/api/admin/";

    public interface KonsultasiApi {
        @GET("consults")
        Call<Base<ArrayList<Consultation>>> getConsults();

        @FormUrlEncoded
        @POST("consults/{id}/answer")
        Call<AfterChangeCallback> postAnswer(
                @Path("id") int id, @Field("jawaban") String jawaban);
    }

    public interface MemberApi {
        @GET("members")
        Call<Base<ArrayList<Member>>> getMembers();

        @GET("members/{id}")
        Call<Base<Member>> getMember(@Path("id") int id);

        @GET("members/{id}/promote")
        Call<AfterChangeCallback> promoteMember(@Path("id") int id);

        @GET("members/{id}/depromote")
        Call<AfterChangeCallback> depromoteMember(@Path("id") int id);

        @GET("members/{id}/delete")
        Call<AfterChangeCallback> deleteMember(@Path("id") int id);

        @FormUrlEncoded
        @POST("members/{id}/update")
        Call<AfterChangeCallback> updateMember(
                @Path("id") int id, @Field("nama") String nama);
    }

    public interface RecipeApi {
        @GET("recipes")
        Call<Base<ArrayList<Recipe>>> getRecipes();

        @Multipart
        @POST("recipes/store")
        Call<AfterChangeCallback> postNewRecipe(
                @Part("nama") RequestBody nama, @Part("kategori") RequestBody kategori,
                @Part("deskripsi") RequestBody deskripsi, @Part("keterangan") RequestBody keterangan,
                @Part("fixed") RequestBody fixed, @Part MultipartBody.Part foto);

        @Multipart
        @POST("recipes/{id}/update")
        Call<AfterChangeCallback> postUpdateRecipe(
                @Path("id") int id, @Part("nama") RequestBody nama, @Part("kategori") RequestBody kategori,
                @Part("deskripsi") RequestBody deskripsi, @Part("keterangan") RequestBody keterangan,
                @Part("fixed") RequestBody fixed, @Part MultipartBody.Part foto);

        @GET("recipes/{id}")
        Call<Base<Recipe>> getRecipeDetail(@Path("id") int id);

        @GET("recipes/{id}/delete")
        Call<AfterChangeCallback> deleteRecipe(@Path("id") int id);
    }

    public interface GejalaApi {
        @GET("gejalas")
        Call<Base<ArrayList<Gejala>>> getGejalas();

        @FormUrlEncoded
        @POST("gejalas/{id}/update")
        Call<AfterChangeCallback> updateGejala(
                @Path("id") int id, @Field("nama") String nama);

        @FormUrlEncoded
        @POST("gejalas/store")
        Call<AfterChangeCallback> storeGejala(
                @Field("nama") String nama);

        @GET("gejalas/{id}/delete")
        Call<AfterChangeCallback> deleteGejala(@Path("id") int id);
    }

    public interface AturanDiagnosis {
        @GET("penyebabs")
        Call<Base<ArrayList<Penyebab>>> getPenyebabs();

        @GET("penyebabs/{id}")
        Call<Base<PenyebabWithAturan>> getPenyebabAturan(@Path("id") int id);

        @GET("penyebabs/{id}/delete")
        Call<AfterChangeCallback> deletePenyebab(@Path("id") int id);

        @FormUrlEncoded
        @POST("penyebabs/{id}/update")
        Call<AfterChangeCallback> updatePenyebab(
                @Path("id") int id, @Field("nama") String nama, @Field("gejalaids") String gejalaids,
                @Field("solusi") String solusi);

        @FormUrlEncoded
        @POST("penyebabs/store")
        Call<AfterChangeCallback> storePenyebab(
                @Field("nama") String nama, @Field("gejalaids") String gejalaids,
                @Field("solusi") String solusi);
    }

    public interface VideoApi {
        @GET("videos")
        Call<Base<ArrayList<Video>>> getVideos();

        @GET("videos/{id}/delete")
        Call<AfterChangeCallback> deleteVideo(@Path("id") int id);

        @FormUrlEncoded
        @POST("videos/{id}/update")
        Call<AfterChangeCallback> updateVideo(
                @Path("id") int id, @Field("url") String url);

        @FormUrlEncoded
        @POST("videos")
        Call<AfterChangeCallback> storeVideo(@Field("url") String url);
    }

    public interface FaqApi {
        @FormUrlEncoded
        @POST("faqs")
        Call<AfterChangeCallback> storeFaqs(@Field("pertanyaan") String pertanyaan,
                                            @Field("jawaban") String jawaban,
                                            @Field("category_name") String cat_name);

        @GET("faqs/categories")
        Call<Base<ArrayList<FaqCategory>>> getFaqCategories();
    }
}
