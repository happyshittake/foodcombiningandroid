package me.bagus.foodcombining.api.models;

import java.util.ArrayList;

public class MenuRecipeDetail {
  public int id;
  public String nama;
  public String kategori;
  public String deskripsi;
  public String image;
  public String keterangan;
  public String created_at;
  public String author;
  public ArrayList<MenuComment> comments;

  @Override public String toString() {
    return "MenuRecipeDetail{" +
        "id=" + id +
        ", nama='" + nama + '\'' +
        ", kategori='" + kategori + '\'' +
        ", deskripsi='" + deskripsi + '\'' +
        ", keterangan='" + keterangan + '\'' +
        ", created_at='" + created_at + '\'' +
        ", comments=" + comments +
        '}';
  }
}
