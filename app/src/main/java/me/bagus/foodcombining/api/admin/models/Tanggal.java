package me.bagus.foodcombining.api.admin.models;

import org.parceler.Parcel;

@Parcel public class Tanggal {
  public String date;
  public int timezone_type;
  public String timezone;
}
