package me.bagus.foodcombining;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Response;
import me.bagus.foodcombining.db.AlarmRecord;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;

public class FoodAlarmReceiverActivity extends AppCompatActivity implements Callback<Response> {
  @BindView(R.id.alarm_receiver_food_name) TextView mFoodName;
  @BindView(R.id.alarm_receiver_time) TextView mTime;
  private AlarmRecord mRecord;
  private User mUser;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_food_alarm_receiver);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    int alarmId = getIntent().getIntExtra("alarm_id", 0);

    if (alarmId < 1) {
      return;
    }

    mRecord = AlarmRecord.findById(AlarmRecord.class, alarmId);

    if (mRecord == null) {
      return;
    }

    mUser = PrefUtils.getCurrentUser(this);

    mFoodName.setText(mRecord.getMakanan());
    mTime.setText(
        String.format(Locale.getDefault(), "%d:%d", mRecord.getHour(), mRecord.getMinute()));
  }

  @OnClick(R.id.alarm_receiver_yes_button) public void onYes(View view) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
    ApiService.getDiaryServices()
        .storeDiary(mRecord.getMakanan(), intToTipeMakan(mRecord.getTipe()),
            format.format(cal.getTime()), mUser.token)
        .enqueue(this);
  }

  @OnClick(R.id.alarm_receiver_no_button) public void onNo(View view) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.putExtra("gotoadddiary", "hello");
    startActivity(intent);
    finish();
  }

  private String intToTipeMakan(int tipe) {
    switch (tipe) {
      case 1:
        return "siang";
      case 2:
        return "malam";
      default:
        return "pagi";
    }
  }

  @Override public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
    Toast.makeText(this, "Diary sudah ditambah", Toast.LENGTH_SHORT).show();
    finish();
  }

  @Override public void onFailure(Call<Response> call, Throwable t) {
    t.printStackTrace();
  }
}
