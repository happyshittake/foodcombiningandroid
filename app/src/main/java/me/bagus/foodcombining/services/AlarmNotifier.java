package me.bagus.foodcombining.services;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import br.com.goncalves.pugnotification.notification.PugNotification;
import me.bagus.foodcombining.FoodAlarmReceiverActivity;
import me.bagus.foodcombining.R;

public class AlarmNotifier extends Service {
  @Nullable @Override public IBinder onBind(Intent intent) {
    return null;
  }

  @Override public int onStartCommand(Intent intent, int flags, int startId) {
    Log.d("receiver", "intent received");
    String message = intent.getStringExtra("MESSAGE");
    int alarmId = intent.getIntExtra("alarm_id", 0);
    Bundle bundle = new Bundle();
    bundle.putInt("alarm_id", alarmId);
    Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    PugNotification.with(this)
        .load()
        .identifier(alarmId)
        .smallIcon(R.drawable.ic_menu_overflow_card)
        .title("Alarm " + message)
        .message("selamat " + message)
        .flags(Notification.DEFAULT_ALL)
        .sound(soundUri)
        .click(FoodAlarmReceiverActivity.class, bundle)
        .autoCancel(true)
        .simple()
        .build();

    return START_STICKY;
  }
}
