package me.bagus.foodcombining.adapters;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

@Parcel public class QuizAnswerHistory {
  public String[] list_jawaban;
  public int idx_jawaban;
  public int idx_pilihan;

  public @ParcelConstructor QuizAnswerHistory(String[] list_jawaban, int idx_jawaban,
      int idx_pilihan) {
    this.list_jawaban = list_jawaban;
    this.idx_jawaban = idx_jawaban;
    this.idx_pilihan = idx_pilihan;
  }
}
