package me.bagus.foodcombining.adapters.admin;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.models.Member;

public class AdminMemberListAdapter extends BaseQuickAdapter<Member> {
  public AdminMemberListAdapter(List<Member> data) {
    super(R.layout.layout_admin_member_list_item, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, Member member) {
    baseViewHolder.setText(R.id.layout_member_list_item_nama, member.nama)
        .setText(R.id.layout_member_list_item_email, member.email);
  }
}
