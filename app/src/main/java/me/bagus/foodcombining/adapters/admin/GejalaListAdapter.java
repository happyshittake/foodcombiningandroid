package me.bagus.foodcombining.adapters.admin;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.models.Gejala;

public class GejalaListAdapter extends BaseQuickAdapter<Gejala> {
  public GejalaListAdapter(List<Gejala> data) {
    super(R.layout.layout_admin_gejala_item, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, Gejala gejala) {
    baseViewHolder.setText(R.id.layout_gejala_item_nama, gejala.nama);
  }
}