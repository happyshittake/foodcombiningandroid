package me.bagus.foodcombining.adapters;

import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.models.Recipe;

public class RecipeListAdapter extends BaseQuickAdapter<Recipe> {
  public RecipeListAdapter(List<Recipe> data) {
    super(R.layout.layout_recipe_list_item, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, Recipe recipe) {
    baseViewHolder.setText(R.id.layout_recipe_author, recipe.author)
        .setText(R.id.layout_recipe_keterangan, recipe.keterangan)
        .setText(R.id.layout_recipe_name, recipe.nama);

    Glide.with(mContext)
        .load(recipe.image)
        .crossFade()
        .centerCrop()
        .into((ImageView) baseViewHolder.getView(R.id.layout_recipe_image));
  }
}
