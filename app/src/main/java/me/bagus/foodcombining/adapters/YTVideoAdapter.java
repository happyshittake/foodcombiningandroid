package me.bagus.foodcombining.adapters;

import android.view.View;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.bagus.foodcombining.DeveloperKey;
import me.bagus.foodcombining.R;

public class YTVideoAdapter extends BaseQuickAdapter<VideoEntry> {
  private final Map<YouTubeThumbnailView, YouTubeThumbnailLoader> mThumbnailLoader;
  private final List<View> mViews;
  private final ThumbnailListener mThumbnailListener;

  public YTVideoAdapter(List<VideoEntry> data) {
    super(R.layout.layout_yt_thumbnail_view, data);
    mThumbnailLoader = new HashMap<>();
    mViews = new ArrayList<>();
    mThumbnailListener = new ThumbnailListener();
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, VideoEntry videoEntry) {
    YouTubeThumbnailView thumbnail = baseViewHolder.getView(R.id.video_thumbnail);
    thumbnail.setTag(videoEntry.videoId);
    thumbnail.initialize(DeveloperKey.DEVELOPER_KEY, mThumbnailListener);
  }

  private final class ThumbnailListener implements YouTubeThumbnailView.OnInitializedListener,
      YouTubeThumbnailLoader.OnThumbnailLoadedListener {

    @Override public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {

    }

    @Override public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView,
        YouTubeThumbnailLoader.ErrorReason errorReason) {

    }

    @Override public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView,
        YouTubeThumbnailLoader youTubeThumbnailLoader) {
      youTubeThumbnailLoader.setOnThumbnailLoadedListener(this);
      mThumbnailLoader.put(youTubeThumbnailView, youTubeThumbnailLoader);
      String videoId = (String) youTubeThumbnailView.getTag();
      youTubeThumbnailLoader.setVideo(videoId);
    }

    @Override public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView,
        YouTubeInitializationResult youTubeInitializationResult) {

    }
  }
}
