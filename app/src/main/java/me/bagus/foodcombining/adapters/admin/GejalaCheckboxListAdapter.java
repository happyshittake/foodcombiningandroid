package me.bagus.foodcombining.adapters.admin;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.ArrayList;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.models.Gejala;

public class GejalaCheckboxListAdapter extends BaseQuickAdapter<Gejala> {
  private List<Gejala> mCheckedGejalas;

  public GejalaCheckboxListAdapter(List<Gejala> data, List<Gejala> checkedGejalas) {
    super(R.layout.layout_gejala_checkbox_item, data);
    mCheckedGejalas = new ArrayList<>();
    for (Gejala gejala : data) {
      for (Gejala checked : checkedGejalas) {
        if (gejala.id == checked.id) {
          mCheckedGejalas.add(gejala);
        }
      }
    }
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, final Gejala gejala) {
    CheckBox checkBox = baseViewHolder.getView(R.id.layout_gejala_checkbox_checkbox);
    checkBox.setOnCheckedChangeListener(null);
    checkBox.setChecked(mCheckedGejalas.contains(gejala));
    baseViewHolder.setOnCheckedChangeListener(R.id.layout_gejala_checkbox_checkbox,
        new CompoundButton.OnCheckedChangeListener() {
          @Override public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
              mCheckedGejalas.add(gejala);
            } else {
              mCheckedGejalas.remove(gejala);
            }
          }
        }).setText(R.id.layout_gejala_checkbox_nama, gejala.nama);
  }

  public String getCheckedIds() {
    StringBuilder sb = new StringBuilder();

    for (Gejala g : mCheckedGejalas) {
      sb.append(g.id);
      sb.append(",");
    }

    return sb.toString();
  }
}
