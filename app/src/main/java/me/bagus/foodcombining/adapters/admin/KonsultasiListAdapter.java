package me.bagus.foodcombining.adapters.admin;

import android.view.View;
import android.widget.Toast;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.models.Consultation;

public class KonsultasiListAdapter extends BaseQuickAdapter<Consultation> {
  public KonsultasiListAdapter(List<Consultation> data) {
    super(R.layout.layout_konsultasi_item2, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, final Consultation consultation) {
    baseViewHolder.setText(R.id.layout_konsultasi_pertanyaan, consultation.pertanyaan)
        .setText(R.id.layout_konsultasi_author, consultation.author)
        .setText(R.id.layout_konsultasi_tanggal, consultation.tanggal.date);

    baseViewHolder.setOnClickListener(R.id.layout_konsultasi_author, new View.OnClickListener() {
      @Override public void onClick(View view) {
        Toast.makeText(mContext, consultation.author, Toast.LENGTH_LONG).show();
      }
    });

    if (consultation.jawaban != null) {
      baseViewHolder.setText(R.id.layout_konsultasi_jawaban, consultation.jawaban);
    }
  }
}