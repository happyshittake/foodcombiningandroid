package me.bagus.foodcombining.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.ApiInterface;
import me.bagus.foodcombining.api.models.SwipeCard;

import java.util.ArrayList;

public class SwipeCardAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<SwipeCard> mSwipeCards;

    public SwipeCardAdapter(Context context, ArrayList<SwipeCard> swipeCards) {
        mContext = context;
        mSwipeCards = swipeCards;
    }

    @Override
    public int getCount() {
        return mSwipeCards.size();
    }

    @Override
    public SwipeCard getItem(int i) {
        return mSwipeCards.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.layout_swipe_card, viewGroup, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.swipe_card_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String imageUrl = ApiInterface.SERVER_BASE_URL + getItem(i).imageUrl;

        Glide.with(mContext).load(imageUrl).into(holder.imageView);

        return view;
    }

    private static class ViewHolder {
        ImageView imageView;
    }
}
