package me.bagus.foodcombining.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.EventDiary;

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.ViewHolder> {
    private ArrayList<EventDiary> mDiaries;
    private Calendar mCal;
    private SimpleDateFormat mParser;
    private SimpleDateFormat mFormatter;

    public DiaryAdapter(ArrayList<EventDiary> diaries) {
        mDiaries = diaries;
        mCal = Calendar.getInstance();
        mParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        mFormatter = new SimpleDateFormat("HH:mm:ss");
    }

    public void swap(ArrayList<EventDiary> diaries) {
        mDiaries.clear();
        mDiaries.addAll(diaries);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.layout_diary_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventDiary diary = mDiaries.get(position);

        holder.textNama.setText(diary.menu);
        holder.textKategori.setText(diary.kategori);

        try {
            Date date = mParser.parse(diary.time);
            holder.textJam.setText(mFormatter.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mDiaries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textNama;
        TextView textKategori;
        TextView textJam;

        public ViewHolder(View itemView) {
            super(itemView);

            textJam = (TextView) itemView.findViewById(R.id.diary_jam);
            textNama = (TextView) itemView.findViewById(R.id.diary_item_nama);
            textKategori = (TextView) itemView.findViewById(R.id.diary_kategori);
        }
    }
}
