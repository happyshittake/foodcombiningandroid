package me.bagus.foodcombining.adapters;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.KonsultasiItem;

public class KonsultasiAdapter extends BaseQuickAdapter<KonsultasiItem> {
  public KonsultasiAdapter(ArrayList<KonsultasiItem> mItems) {
    super(R.layout.layout_konsultasi_item, mItems);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, KonsultasiItem konsultasiItem) {
    baseViewHolder.setText(R.id.konsultasi_jawaban, konsultasiItem.jawaban)
        .setText(R.id.konsultasi_pertanyaan, konsultasiItem.pertanyaan)
        .setText(R.id.konsultasi_author, konsultasiItem.author)
        .setText(R.id.konsultasi_tanggal, konsultasiItem.tanggal);
  }
}
