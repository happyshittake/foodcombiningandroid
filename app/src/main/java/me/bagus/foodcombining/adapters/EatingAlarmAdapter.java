package me.bagus.foodcombining.adapters;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.db.AlarmRecord;
import me.bagus.foodcombining.services.AlarmNotifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EatingAlarmAdapter extends RecyclerView.Adapter<EatingAlarmAdapter.ViewHolder> {
    private List<AlarmRecord> alarmRecords;
    private Context mContext;

    public EatingAlarmAdapter(List<AlarmRecord> alarmRecords) {
        this.alarmRecords = alarmRecords;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View alarmView;

        if (viewType == 0) {
            alarmView = inflater.inflate(R.layout.layout_alarm_item, parent, false);
        } else {
            alarmView = inflater.inflate(R.layout.layout_dark_alarm_item, parent, false);
        }

        return new ViewHolder(alarmView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final AlarmRecord alarmRecord = alarmRecords.get(position);
        String tipe;
        if (alarmRecord.getTipe() == 0) {
            tipe = "PAGI";
        } else if (alarmRecord.getTipe() == 1) {
            tipe = "SIANG";
        } else {
            tipe = "MALAM";
        }

        holder.alarmMakanan.setText(alarmRecord.getMakanan());
        holder.alarmTipe.setText(tipe);
        holder.delAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, AlarmNotifier.class);
                PendingIntent pi = PendingIntent.getActivity(mContext, alarmRecord.getId().intValue(), intent, 0);
                pi.cancel();
                AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
                am.cancel(pi);
                alarmRecords.remove(position);
                alarmRecord.delete();
                EatingAlarmAdapter.this.notifyDataSetChanged();
            }
        });

        holder.alarmTime.setText(String.format(Locale.getDefault(), "%d:%d", alarmRecord.getHour(), alarmRecord.getMinute()));
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return 0;
        }

        return 1;
    }

    @Override
    public int getItemCount() {
        return alarmRecords.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView alarmMakanan;
        TextView alarmTipe;
        ImageButton delAlarm;
        TextView alarmTime;

        public ViewHolder(View itemView) {
            super(itemView);

            alarmMakanan = (TextView) itemView.findViewById(R.id.alarm_makanan);
            alarmTipe = (TextView) itemView.findViewById(R.id.alarm_tipe);
            delAlarm = (ImageButton) itemView.findViewById(R.id.alarm_delete);
            alarmTime = (TextView) itemView.findViewById(R.id.alarm_time);
        }
    }
}
