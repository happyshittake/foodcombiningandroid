package me.bagus.foodcombining.adapters;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;

public class QuizResultAdapter extends BaseQuickAdapter<QuizAnswerHistory> {
  public QuizResultAdapter(List<QuizAnswerHistory> data) {
    super(R.layout.layout_quiz_result, data);
  }

  @Override
  protected void convert(BaseViewHolder baseViewHolder, QuizAnswerHistory quizAnswerHistory) {
    baseViewHolder.setText(R.id.layout_quiz_result_real_answer,
        "Jawaban benar: " + quizAnswerHistory.list_jawaban[quizAnswerHistory.idx_jawaban])
        .setText(R.id.layout_quiz_result_user_anwer,
            "Jawaban anda: " + quizAnswerHistory.list_jawaban[quizAnswerHistory.idx_pilihan]);
  }
}
