package me.bagus.foodcombining.adapters;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;

public class GameResultAdapter extends BaseQuickAdapter<AnswerHistory> {
  public GameResultAdapter(List<AnswerHistory> data) {
    super(R.layout.layout_game_result_answer, data);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, AnswerHistory answerHistory) {
    baseViewHolder.setText(R.id.user_anwer, "Jawaban anda: " + answerHistory.userAnser)
        .setText(R.id.card_answer, "Jawaban benar: " + answerHistory.realAnswer);
    String result;

    if (answerHistory.realAnswer.equalsIgnoreCase(answerHistory.userAnser)) {
      result = "Benar";
    } else {
      result = "Salah";
    }

    baseViewHolder.setText(R.id.result_anwer, result);
  }
}
