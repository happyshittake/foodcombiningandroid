package me.bagus.foodcombining.adapters;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

@Parcel public class AnswerHistory {
  public String realAnswer;
  public String userAnser;

  public @ParcelConstructor AnswerHistory(String realAnswer, String userAnser) {
    this.realAnswer = realAnswer;
    this.userAnser = userAnser;
  }
}
