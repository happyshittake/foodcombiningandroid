package me.bagus.foodcombining.adapters;

import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.SectionEntity;

import java.util.ArrayList;
import java.util.Locale;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.Faq;

public class FaqAdapter extends BaseSectionQuickAdapter<SectionWrapper.SectionWrapperEntity> {
    private TextToSpeech mTts;

    public FaqAdapter(ArrayList<Faq> faqs, TextToSpeech tts) {
        super(R.layout.layout_faq_item, R.layout.faq_section_header,
                new SectionWrapper(faqs).getSections());
        this.mTts = tts;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTts.setLanguage(Locale.forLanguageTag("ind-IND"));
        } else {
            mTts.setLanguage(new Locale("in_ID"));
        }
    }

    @Override
    protected void convertHead(BaseViewHolder baseViewHolder,
                               SectionWrapper.SectionWrapperEntity sectionWrapperEntity) {
        baseViewHolder.setText(R.id.faq_section_header_text, sectionWrapperEntity.header);
    }

    @Override
    protected int getDefItemViewType(int position) {
        if (((SectionEntity) this.mData.get(position)).isHeader) {
            return 1092;
        }

        return position % 2 == 0 ? 4 : 3;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder,
                           final SectionWrapper.SectionWrapperEntity sectionWrapperEntity) {
        baseViewHolder.setText(R.id.item_faq_answer_text, sectionWrapperEntity.t.jawaban)
                .setText(R.id.item_faq_question_text, sectionWrapperEntity.t.pertanyaan);

        if (baseViewHolder.getItemViewType() == 4) {
            baseViewHolder.convertView.setBackgroundColor(
                    ContextCompat.getColor(mContext, R.color.gray));
        }

        baseViewHolder.setOnClickListener(R.id.item_faq_speak, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Clicked", "CLicked");
                if (mTts.isSpeaking()) {
                    mTts.stop();
                }

                mTts.speak(sectionWrapperEntity.t.jawaban, TextToSpeech.QUEUE_FLUSH, null);
            }
        });
    }
}
