package me.bagus.foodcombining.adapters;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;
import java.util.Locale;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.QuizScore;

public class QuizScoreAdapter extends BaseQuickAdapter<QuizScore> {
    public QuizScoreAdapter(List<QuizScore> data) {
        super(R.layout.layout_quiz_score_item, data);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, QuizScore quizScore) {
        baseViewHolder.setText(R.id.layout_quiz_score_author, quizScore.author)
                .setText(R.id.layout_quiz_score_score,
                        String.format(Locale.getDefault(), "Score: %d", quizScore.score))
                .setText(R.id.layout_quiz_score_tanggal, quizScore.tanggal);
    }
}
