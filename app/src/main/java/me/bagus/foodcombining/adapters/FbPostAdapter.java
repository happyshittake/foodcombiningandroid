package me.bagus.foodcombining.adapters;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.Post;

public class FbPostAdapter extends BaseQuickAdapter<Post> {
  public FbPostAdapter(List<Post> data) {
    super(R.layout.layout_fb_post_item, data);
  }

  @Override public int getItemViewType(int position) {
    return position % 2 == 0 ? 4 : 3;
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, Post post) {
    baseViewHolder.setText(R.id.item_post_text, post.content);

    if (baseViewHolder.getItemViewType() == 4) {
      baseViewHolder.getConvertView()
          .setBackgroundColor(ContextCompat.getColor(mContext, R.color.gray));
    }

    Glide.with(mContext)
        .load(post.image_url)
        .crossFade()
        .centerCrop()
        .into((ImageView) baseViewHolder.getView(R.id.item_post_image));
  }
}
