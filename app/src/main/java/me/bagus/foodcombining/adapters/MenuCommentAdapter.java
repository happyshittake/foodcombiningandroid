package me.bagus.foodcombining.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.MenuComment;

public class MenuCommentAdapter extends RecyclerView.Adapter<MenuCommentAdapter.ViewHolder> {
    private ArrayList<MenuComment> mComments;

    public MenuCommentAdapter(ArrayList<MenuComment> mComments) {
        this.mComments = mComments;
    }

    public void swap(ArrayList<MenuComment> comments) {
        mComments.clear();
        mComments.addAll(comments);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.layout_comment_item, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuComment comment = mComments.get(position);

        holder.textDate.setText(comment.created_at);
        holder.textNama.setText(comment.user.nama);
        holder.textContent.setText(comment.content);
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textNama;
        TextView textDate;
        TextView textContent;

        public ViewHolder(View itemView) {
            super(itemView);
            textContent = (TextView) itemView.findViewById(R.id.comment_item_content);
            textNama = (TextView) itemView.findViewById(R.id.comment_item_nama);
            textDate = (TextView) itemView.findViewById(R.id.comment_item_tanggal);
        }
    }
}
