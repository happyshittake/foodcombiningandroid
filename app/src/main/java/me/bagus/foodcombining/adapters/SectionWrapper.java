package me.bagus.foodcombining.adapters;

import com.chad.library.adapter.base.entity.SectionEntity;
import java.util.ArrayList;
import java.util.HashMap;
import me.bagus.foodcombining.api.models.Faq;

public class SectionWrapper {
  private HashMap<String, ArrayList<Faq>> mGroupedData;

  public SectionWrapper(ArrayList<Faq> data) {
    mGroupedData = new HashMap<>();
    for (Faq f : data) {
      if (!mGroupedData.containsKey(f.kategori)) {
        ArrayList<Faq> list = new ArrayList<>();
        list.add(f);
        mGroupedData.put(f.kategori, list);
      } else {
        mGroupedData.get(f.kategori).add(f);
      }
    }
  }

  public ArrayList<SectionWrapperEntity> getSections() {
    ArrayList<SectionWrapperEntity> list = new ArrayList<>();

    for (String section : mGroupedData.keySet()) {
      list.add(new SectionWrapperEntity(true, section));
      for (Faq faq : mGroupedData.get(section)) {
        list.add(new SectionWrapperEntity(faq));
      }
    }

    return list;
  }

  public class SectionWrapperEntity extends SectionEntity<Faq> {

    public SectionWrapperEntity(boolean isHeader, String header) {
      super(isHeader, header);
    }

    public SectionWrapperEntity(Faq faq) {
      super(faq);
    }
  }
}
