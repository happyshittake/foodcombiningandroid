package me.bagus.foodcombining.adapters;

import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.MenuRecipe;

public class FoodMenuAdapter extends BaseQuickAdapter<MenuRecipe> {

  public FoodMenuAdapter(ArrayList<MenuRecipe> menuRecipes) {
    super(R.layout.layout_menu_item, menuRecipes);
  }

  @Override protected void convert(BaseViewHolder baseViewHolder, MenuRecipe menuRecipe) {
    baseViewHolder.setText(R.id.menu_item_nama, menuRecipe.nama)
        .setText(R.id.menu_item_keterangan, menuRecipe.keterangan)
        .setText(R.id.menu_item_author, menuRecipe.author);

    Glide.with(mContext)
        .load(menuRecipe.image)
        .crossFade()
        .placeholder(ContextCompat.getDrawable(mContext, R.drawable.no_image_placeholder))
        .centerCrop()
        .into((ImageView) baseViewHolder.getView(R.id.menu_item_image));
  }
}
