package me.bagus.foodcombining.adapters.admin;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.AdminApi;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Video;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoAdapter extends BaseQuickAdapter<Video> implements Callback<AfterChangeCallback> {
  private MaterialDialog materialDialog;
  private AdminApi.VideoApi videoApi;

  public VideoAdapter(List<Video> data) {
    super(R.layout.layout_video_item, data);
    videoApi = AdminApiService.getVideoApiServices();
  }

  @Override protected void convert(final BaseViewHolder baseViewHolder, final Video video) {
    baseViewHolder.setText(R.id.video_id_field, video.url);
    baseViewHolder.setOnClickListener(R.id.video_del, new View.OnClickListener() {
      @Override public void onClick(View view) {
        materialDialog =
            new MaterialDialog.Builder(mContext).content("Confirm delete video id=" + video.url)
                .positiveText("Yes")
                .negativeText("Cancel")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                  @Override
                  public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    videoApi.deleteVideo(video.id).enqueue(VideoAdapter.this);
                    deleteDataById(video.id);
                  }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                  @Override
                  public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                  }
                })
                .build();
        materialDialog.show();
      }
    });

    baseViewHolder.setOnClickListener(R.id.video_save, new View.OnClickListener() {
      @Override public void onClick(View view) {
        materialDialog = new MaterialDialog.Builder(mContext).content("Saving ....").build();
        materialDialog.show();
        EditText field = baseViewHolder.getView(R.id.video_id_field);
        videoApi.updateVideo(video.id, field.getText().toString()).enqueue(VideoAdapter.this);
      }
    });
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    if (materialDialog != null) {
      materialDialog.dismiss();
    }

    Toast.makeText(mContext, "operasi sukses", Toast.LENGTH_SHORT).show();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {

  }

  private void deleteDataById(int id) {
    for (Video video : getData()) {
      if (video.id == id) {
        getData().remove(video);
        notifyDataSetChanged();
        return;
      }
    }
  }
}
