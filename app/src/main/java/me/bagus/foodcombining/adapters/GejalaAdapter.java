package me.bagus.foodcombining.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.models.Gejala;

/**
 * Created by ndjoe on 27/06/16.
 */
public class GejalaAdapter extends RecyclerView.Adapter<GejalaAdapter.ViewHolder> {
    private ArrayList<Gejala> mGejalas;
    private ArrayList<Integer> mCheckedGejalaIds;

    public GejalaAdapter(ArrayList<Gejala> mGejalas) {
        this.mGejalas = mGejalas;
        mCheckedGejalaIds = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = View.inflate(context, R.layout.layout_gejala_item, null);

        return new ViewHolder(view);
    }

    public String getCheckedIdxsString() {
        return android.text.TextUtils.join(",", mCheckedGejalaIds);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Gejala gejala = mGejalas.get(position);
        holder.checkBox.setOnCheckedChangeListener(null);

        holder.nameText.setText(gejala.nama);
        holder.checkBox.setChecked(mCheckedGejalaIds.contains(gejala.id));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mCheckedGejalaIds.add(gejala.id);
                } else {
                    mCheckedGejalaIds.remove((Integer) gejala.id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGejalas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameText;
        private CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.layout_gejala_item_nama);
            checkBox = (CheckBox) itemView.findViewById(R.id.layout_gejala_item_checkbox);
        }
    }
}
