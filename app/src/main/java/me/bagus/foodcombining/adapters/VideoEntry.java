package me.bagus.foodcombining.adapters;

public final class VideoEntry {
  public String videoId;

  public VideoEntry(String videoId) {
    this.videoId = videoId;
  }
}
