package me.bagus.foodcombining.db;

import com.orm.SugarRecord;

public class AlarmRecord extends SugarRecord {
    private long id;
    private int tipe;
    private String makanan;
    private int hour;
    private int minute;

    public AlarmRecord() {

    }

    public AlarmRecord(int tipe, String makanan, int hour, int minute) {
        this.tipe = tipe;
        this.makanan = makanan;
        this.hour = hour;
        this.minute = minute;
    }

    public int getTipe() {
        return tipe;
    }

    public String getMakanan() {
        return makanan;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }
}
