package me.bagus.foodcombining;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Register;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FbLoginActivity extends AppCompatActivity
    implements FacebookCallback<LoginResult>, Callback<Register> {

  private LoginButton mLoginButton;
  private CallbackManager mCallbackManager;
  private User mUser;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    setContentView(R.layout.activity_fb_login);

    if (PrefUtils.getCurrentUser(this) != null) {
      Intent homeIntent = new Intent(FbLoginActivity.this, MainActivity.class);

      startActivity(homeIntent);

      finish();
    }
  }

  @Override protected void onResume() {
    super.onResume();
    mLoginButton = (LoginButton) findViewById(R.id.fb_login_button);

    mCallbackManager = CallbackManager.Factory.create();
    mLoginButton.registerCallback(mCallbackManager, this);

    mLoginButton.setReadPermissions("public_profile", "email");

    if (PrefUtils.getCurrentUser(this) != null) {
      Intent homeIntent = new Intent(this, MainActivity.class);

      startActivity(homeIntent);

      finish();
    }
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    mCallbackManager.onActivityResult(requestCode, resultCode, data);
  }

  @Override public void onSuccess(LoginResult loginResult) {
    mLoginButton.setVisibility(View.GONE);
    Log.d("DEBUG", loginResult.getAccessToken().toString());
    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
        new GraphRequest.GraphJSONObjectCallback() {
          @Override public void onCompleted(JSONObject object, GraphResponse response) {
            try {
              registerUser(object.getString("name"), object.getString("email"),
                  object.getString("id"));
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });

    Bundle parameters = new Bundle();
    parameters.putString("fields", "id,name,email");
    request.setParameters(parameters);
    request.executeAsync();
  }

  private void registerUser(String nama, String email, String token) {
    Log.d("DEBUG_TAG", nama + " " + email + " " + token);
    ApiService.getUserServices().registerUser(nama, email, token).enqueue(this);
  }

  @Override public void onCancel() {

  }

  @Override public void onError(FacebookException error) {

  }

  @Override public void onResponse(Call<Register> call, Response<Register> response) {
    mUser = new User();

    mUser.email = response.body().email;
    mUser.nama = response.body().nama;
    mUser.token = response.body().id;
    mUser.isAdmin = response.body().admin;

    PrefUtils.setCurrentUser(mUser, FbLoginActivity.this);

    if (mUser.isAdmin) {
      Intent intent = new Intent(FbLoginActivity.this, ModePickerActivity.class);
      startActivity(intent);
      finish();
    } else {
      Intent intent = new Intent(FbLoginActivity.this, MainActivity.class);
      startActivity(intent);
      finish();
    }
  }

  @Override public void onFailure(Call<Register> call, Throwable t) {
    t.printStackTrace();
  }
}
