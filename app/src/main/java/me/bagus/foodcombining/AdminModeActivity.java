package me.bagus.foodcombining;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import me.bagus.foodcombining.admin.activities.AddGejalaActivity;
import me.bagus.foodcombining.admin.activities.AddPenyebabActivity;
import me.bagus.foodcombining.admin.activities.AddRecipeActivity;
import me.bagus.foodcombining.admin.activities.AnswerQuestionActivity;
import me.bagus.foodcombining.admin.activities.EditGejalaActivity;
import me.bagus.foodcombining.admin.activities.EditMemberActivity;
import me.bagus.foodcombining.admin.activities.EditPenyebabActivity;
import me.bagus.foodcombining.admin.activities.EditRecipeActivity;
import me.bagus.foodcombining.fragments.admin.AdminMemberMenuFragment;
import me.bagus.foodcombining.fragments.admin.AturanFragment;
import me.bagus.foodcombining.fragments.admin.FaqFragment;
import me.bagus.foodcombining.fragments.admin.GejalaFragment;
import me.bagus.foodcombining.fragments.admin.KonsultasiFragment;
import me.bagus.foodcombining.fragments.admin.RecipeMenuFragment;
import me.bagus.foodcombining.fragments.admin.VideoFragment;
import me.bagus.foodcombining.interfaces.admin.FragmentAction;
import me.bagus.foodcombining.interfaces.admin.FragmentNavigation;
import me.bagus.foodcombining.utils.PrefUtils;

public class AdminModeActivity extends AppCompatActivity
        implements Drawer.OnDrawerItemClickListener, FragmentNavigation, FragmentAction {
    private static final String VIDEO_FRAGMENT_TAG = "videofragment";
    private static final int EDIT_MEMBER_CODE = 001;
    private static final int EDIT_RECIPE_CODE = 002;
    private static final int ADD_RECIPE_CODE = 003;
    private static final int ANSWER_QUESTION = 004;
    private static final int ADD_GEJALA = 005;
    private static final int EDIT_GEJALA = 006;
    private static final int ADD_PENYEBAB = 007;
    private static final int EDIT_PENYEBAB = 010;
    private static final String MEMBER_MENU_FRAGMENT_TAG = "menufragment";
    private static final String RECIPE_MENU_FRAGMENT_TAG = "recipefragment";
    private static final String CONSULTATION_MENU_FRAGMENT_TAG = "konsultasifragment";
    private static final String GEJALA_MENU_FRAGMENT_TAG = "gejalafragment";
    private static final String ATURAN_MENU_FRAGMENT_TAG = "aturanfragment";
    private static final String FAQ_FRAGMENT_TAG = "fagfragment";
    private Drawer mDrawer;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_mode);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mFragmentManager = getSupportFragmentManager();

        setUpDrawer(toolbar);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    private void setUpDrawer(Toolbar toolbar) {
        PrimaryDrawerItem memberMenu = new PrimaryDrawerItem().withIdentifier(1).withName("Member");
        PrimaryDrawerItem recipeMenu = new PrimaryDrawerItem().withIdentifier(2).withName("Recipe");
        PrimaryDrawerItem konsultasiMenu =
                new PrimaryDrawerItem().withIdentifier(3).withName("Konsultasi");
        PrimaryDrawerItem gejalaMenu = new PrimaryDrawerItem().withIdentifier(4).withName("Gejala");
        PrimaryDrawerItem aturanMenu = new PrimaryDrawerItem().withIdentifier(5).withName("Aturan");
        PrimaryDrawerItem changemode =
                new PrimaryDrawerItem().withIdentifier(6).withName("Change Mode");
        PrimaryDrawerItem videoMenu = new PrimaryDrawerItem().withIdentifier(8).withName("Videos");
        PrimaryDrawerItem faqMenu = new PrimaryDrawerItem().withIdentifier(9).withName("Faqs");
        PrimaryDrawerItem logout = new PrimaryDrawerItem().withIdentifier(7).withName("Logout");

        mDrawer = new DrawerBuilder().withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .addDrawerItems(memberMenu, new DividerDrawerItem(), recipeMenu, new DividerDrawerItem(),
                        konsultasiMenu, new DividerDrawerItem(), gejalaMenu, new DividerDrawerItem(),
                        aturanMenu, new DividerDrawerItem(), changemode, new DividerDrawerItem(), videoMenu,
                        new DividerDrawerItem(), faqMenu,
                        new DividerDrawerItem(), logout, new DividerDrawerItem())
                .withOnDrawerItemClickListener(this)
                .build();

        mDrawer.setSelection(1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String operation;
            AdminMemberMenuFragment memberMenuFragment;
            RecipeMenuFragment recipeMenuFragment;
            KonsultasiFragment konsultasiFragment;
            GejalaFragment gejalaFragment;
            AturanFragment aturanFragment;
            switch (requestCode) {
                case EDIT_MEMBER_CODE:
                    operation = data.getStringExtra("operation");

                    if (operation.equalsIgnoreCase("update")) {
                        Toast.makeText(this, "Member berhasil diupdate", Toast.LENGTH_SHORT).show();
                    } else if (operation.equalsIgnoreCase("delete")) {
                        Toast.makeText(this, "Member berhasil dihapus", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Member berhasil dipromote", Toast.LENGTH_SHORT).show();
                    }
                    memberMenuFragment = (AdminMemberMenuFragment) mFragmentManager.findFragmentByTag(
                            MEMBER_MENU_FRAGMENT_TAG);

                    memberMenuFragment.refreshData();
                    break;
                case EDIT_RECIPE_CODE:
                    operation = data.getStringExtra("operation");

                    if (operation.equalsIgnoreCase("update")) {
                        Toast.makeText(this, "Resep berhasil diupdate", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Resep berhasil dihapus", Toast.LENGTH_SHORT).show();
                    }

                    recipeMenuFragment =
                            (RecipeMenuFragment) mFragmentManager.findFragmentByTag(RECIPE_MENU_FRAGMENT_TAG);

                    recipeMenuFragment.refreshData();
                    break;
                case ADD_RECIPE_CODE:
                    Toast.makeText(this, "Resep berhasil ditambah", Toast.LENGTH_SHORT).show();

                    recipeMenuFragment =
                            (RecipeMenuFragment) mFragmentManager.findFragmentByTag(RECIPE_MENU_FRAGMENT_TAG);

                    recipeMenuFragment.refreshData();
                    break;
                case ANSWER_QUESTION:
                    Toast.makeText(this, "Jawaban berhasil disimpan", Toast.LENGTH_SHORT).show();

                    konsultasiFragment = (KonsultasiFragment) mFragmentManager.findFragmentByTag(
                            CONSULTATION_MENU_FRAGMENT_TAG);

                    konsultasiFragment.refreshData();
                    break;
                case ADD_GEJALA:
                    Toast.makeText(this, "Gejala berhasil disimpan", Toast.LENGTH_SHORT).show();

                    gejalaFragment =
                            (GejalaFragment) mFragmentManager.findFragmentByTag(GEJALA_MENU_FRAGMENT_TAG);

                    gejalaFragment.refreshData();
                    break;
                case EDIT_GEJALA:
                    operation = data.getStringExtra("operation");

                    if (operation.equalsIgnoreCase("update")) {
                        Toast.makeText(this, "Gejala berhasil diupdate", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Gejala berhasil dihapus", Toast.LENGTH_SHORT).show();
                    }

                    gejalaFragment =
                            (GejalaFragment) mFragmentManager.findFragmentByTag(GEJALA_MENU_FRAGMENT_TAG);

                    gejalaFragment.refreshData();
                    break;
                case ADD_PENYEBAB:
                    Toast.makeText(this, "Penyebab berhasil disimpan", Toast.LENGTH_SHORT).show();

                    aturanFragment =
                            (AturanFragment) mFragmentManager.findFragmentByTag(ATURAN_MENU_FRAGMENT_TAG);

                    aturanFragment.refreshData();
                    break;
                case EDIT_PENYEBAB:
                    operation = data.getStringExtra("operation");

                    if (operation.equalsIgnoreCase("update")) {
                        Toast.makeText(this, "Penyebab berhasil diupdate", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Penyebab berhasil dihapus", Toast.LENGTH_SHORT).show();
                    }

                    aturanFragment =
                            (AturanFragment) mFragmentManager.findFragmentByTag(ATURAN_MENU_FRAGMENT_TAG);

                    aturanFragment.refreshData();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        switch ((int) drawerItem.getIdentifier()) {
            case 1:
                gotoMemberMenuFragment();
                break;
            case 2:
                gotoRecipeMenuFragment();
                break;
            case 3:
                gotoKonsultasiMenuFragment();
                break;
            case 4:
                gotoGejalaMenuFragment();
                break;
            case 5:
                gotoAturanFragment();
                break;
            case 6:
                Intent intent = new Intent(this, ModePickerActivity.class);
                startActivity(intent);
                finish();
                break;
            case 7:
                PrefUtils.clearCurrentUser(this);
                Intent logoutintent = new Intent(this, FbLoginActivity.class);
                startActivity(logoutintent);
                finish();
                break;
            case 8:
                gotoVideoFragment();
                break;
            case 9:
                gotoFaqFragment();
                break;
        }

        return false;
    }

    @Override
    public void gotoMemberMenuFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, new AdminMemberMenuFragment(), MEMBER_MENU_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void gotoRecipeMenuFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, new RecipeMenuFragment(), RECIPE_MENU_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void gotoKonsultasiMenuFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, new KonsultasiFragment(), CONSULTATION_MENU_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void gotoGejalaMenuFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, new GejalaFragment(), GEJALA_MENU_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void gotoAturanFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, new AturanFragment(), ATURAN_MENU_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void gotoVideoFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, new VideoFragment(), VIDEO_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void gotoFaqFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.admin_fragment, FaqFragment.newInstance(), FAQ_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void editMember(Parcelable parcelable) {
        Intent intent = new Intent(this, EditMemberActivity.class);

        intent.putExtra("parcel", parcelable);

        startActivityForResult(intent, EDIT_MEMBER_CODE);
    }

    @Override
    public void editRecipe(Parcelable parcelable) {
        Intent intent = new Intent(this, EditRecipeActivity.class);

        intent.putExtra("parcel", parcelable);

        startActivityForResult(intent, EDIT_RECIPE_CODE);
    }

    @Override
    public void addRecipe() {
        Intent intent = new Intent(this, AddRecipeActivity.class);

        startActivityForResult(intent, ADD_RECIPE_CODE);
    }

    @Override
    public void answerConsultationQuestion(Parcelable parcelable) {
        Intent intent = new Intent(this, AnswerQuestionActivity.class);

        intent.putExtra("parcel", parcelable);

        startActivityForResult(intent, ANSWER_QUESTION);
    }

    @Override
    public void addGejala() {
        Intent intent = new Intent(this, AddGejalaActivity.class);
        startActivityForResult(intent, ADD_GEJALA);
    }

    @Override
    public void editGejala(Parcelable parcelable) {
        Intent intent = new Intent(this, EditGejalaActivity.class);

        intent.putExtra("parcel", parcelable);

        startActivityForResult(intent, EDIT_GEJALA);
    }

    @Override
    public void addPenyebab() {
        Intent intent = new Intent(this, AddPenyebabActivity.class);

        startActivityForResult(intent, ADD_PENYEBAB);
    }

    @Override
    public void editPenyebab(Parcelable parcelable) {
        Intent intent = new Intent(this, EditPenyebabActivity.class);
        intent.putExtra("parcel", parcelable);

        startActivityForResult(intent, EDIT_PENYEBAB);
    }
}
