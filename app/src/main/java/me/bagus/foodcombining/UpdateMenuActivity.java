package me.bagus.foodcombining;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import java.io.File;
import java.util.List;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.MenuRecipeDetail;
import me.bagus.foodcombining.utils.RequestBodyFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMenuActivity extends AppCompatActivity implements ImagePickerCallback {
  @BindView(R.id.activity_update_menu_deskripsi) EditText mDeskripsiField;
  @BindView(R.id.activity_update_menu_kategori) Spinner mKategoriSpinner;
  @BindView(R.id.activity_update_menu_keterangan) EditText mKeteranganField;
  @BindView(R.id.activity_update_menu_nama) EditText mNamaField;
  @BindView(R.id.activity_update_menu_image_viewer) ImageView mImagePreview;

  private int mMenuId;
  private ImagePicker imagePicker;
  private File mSelectedFile;
  private String mOperation;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_update_menu);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    mMenuId = getIntent().getIntExtra("menu_id", 0);
    ApiService.getFoodMenuServices()
        .getFoodMenuDetail(mMenuId)
        .enqueue(new Callback<Base<MenuRecipeDetail>>() {
          @Override public void onResponse(Call<Base<MenuRecipeDetail>> call,
              Response<Base<MenuRecipeDetail>> response) {
            MenuRecipeDetail menuRecipeDetail = response.body().data;
            mNamaField.setText(menuRecipeDetail.nama);
            mKeteranganField.setText(menuRecipeDetail.keterangan);
            mDeskripsiField.setText(menuRecipeDetail.deskripsi);
            setUpSpinner(menuRecipeDetail.kategori);

            Glide.with(UpdateMenuActivity.this)
                .load(menuRecipeDetail.image)
                .crossFade()
                .placeholder(ContextCompat.getDrawable(UpdateMenuActivity.this,
                    R.drawable.no_image_placeholder))
                .centerCrop()
                .into(mImagePreview);
          }

          @Override public void onFailure(Call<Base<MenuRecipeDetail>> call, Throwable t) {
            t.printStackTrace();
          }
        });
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      if (requestCode == Picker.PICK_IMAGE_DEVICE) {
        if (imagePicker == null) {
          imagePicker = new ImagePicker(this);
          imagePicker.setImagePickerCallback(this);
        }
        imagePicker.submit(data);
      }
    }
  }

  @OnClick(R.id.activity_update_menu_pick_photo_button) public void pickImage(View view) {
    final CharSequence[] items = {
        "Choose from Library", "Cancel"
    };
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Add Photo!");
    builder.setItems(items, new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int item) {
        if (items[item].equals("Choose from Library")) {
          pickImage();
          dialog.dismiss();
        } else if (items[item].equals("Cancel")) {
          dialog.dismiss();
        }
      }
    });
    builder.show();
  }

  @OnClick(R.id.activity_update_menu_save_button) public void saveMenu(View view) {
    mOperation = "update";
    ApiService.getFoodMenuServices()
        .postUpdateRecipe(RequestBodyFactory.createPartFromString(mNamaField.getText().toString()),
            RequestBodyFactory.createPartFromString(mDeskripsiField.getText().toString()),
            RequestBodyFactory.createPartFromString(mKategoriSpinner.getSelectedItem().toString()),
            RequestBodyFactory.createPartFromString(mKeteranganField.getText().toString()),
            RequestBodyFactory.prepareFilePart("foto", mSelectedFile), mMenuId)
        .enqueue(new Callback<me.bagus.foodcombining.api.models.Response>() {
          @Override public void onResponse(Call<me.bagus.foodcombining.api.models.Response> call,
              Response<me.bagus.foodcombining.api.models.Response> response) {
            Intent intent = new Intent();
            intent.putExtra("operation", mOperation);
            setResult(RESULT_OK, intent);
            finish();
          }

          @Override public void onFailure(Call<me.bagus.foodcombining.api.models.Response> call,
              Throwable t) {
            t.printStackTrace();
          }
        });
  }

  @OnClick(R.id.activity_update_menu_delete_button) public void deleteMenu(View view) {
    mOperation = "delete";
    ApiService.getFoodMenuServices()
        .deleteMenu(mMenuId)
        .enqueue(new Callback<me.bagus.foodcombining.api.models.Response>() {
          @Override public void onResponse(Call<me.bagus.foodcombining.api.models.Response> call,
              Response<me.bagus.foodcombining.api.models.Response> response) {
            Intent intent = new Intent();
            intent.putExtra("operation", mOperation);
            setResult(RESULT_OK, intent);
            finish();
          }

          @Override public void onFailure(Call<me.bagus.foodcombining.api.models.Response> call,
              Throwable t) {
            t.printStackTrace();
          }
        });
  }

  private void pickImage() {
    imagePicker = new ImagePicker(this);
    imagePicker.setRequestId(1234);
    imagePicker.ensureMaxSize(5090, 5090);
    imagePicker.shouldGenerateMetadata(true);
    imagePicker.shouldGenerateThumbnails(true);
    imagePicker.setImagePickerCallback(this);
    Bundle bundle = new Bundle();
    bundle.putInt("android.intent.extras.CAMERA_FACING", 1);
    imagePicker.pickImage();
  }

  private void setUpSpinner(String kategori) {
    ArrayAdapter<CharSequence> adapter =
        ArrayAdapter.createFromResource(this, R.array.list_tipe_makan,
            android.R.layout.simple_spinner_item);

    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    mKategoriSpinner.setAdapter(adapter);

    if (kategori.equalsIgnoreCase("pagi")) {
      mKategoriSpinner.setSelection(0);
    } else if (kategori.equalsIgnoreCase("siang")) {
      mKategoriSpinner.setSelection(1);
    } else {
      mKategoriSpinner.setSelection(2);
    }
  }

  @Override public void onImagesChosen(List<ChosenImage> list) {
    for (ChosenImage image : list) {
      mSelectedFile = new File(image.getOriginalPath());

      Glide.with(this).load(mSelectedFile).crossFade().centerCrop().into(mImagePreview);
    }
  }

  @Override public void onError(String s) {
    Log.e("error", s);
  }
}
