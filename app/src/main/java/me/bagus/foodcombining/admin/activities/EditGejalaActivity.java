package me.bagus.foodcombining.admin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Gejala;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditGejalaActivity extends AppCompatActivity implements Callback<AfterChangeCallback> {
  @BindView(R.id.edit_gejala_nama_field) EditText mNamaField;
  private Gejala mGejala;
  private MaterialDialog mProgressDialog;
  private String mOperation;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_gejala);
    ButterKnife.bind(this);

    mGejala = Parcels.unwrap(getIntent().getParcelableExtra("parcel"));

    if (mGejala.nama != null) {
      mNamaField.setText(mGejala.nama);
    }
  }

  @OnClick(R.id.edit_gejala_save_button) public void onGejalaSave(View view) {
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Saving ...").build();

    mProgressDialog.show();
    mOperation = "update";
    AdminApiService.getGejalaApiServices()
        .updateGejala(mGejala.id, mNamaField.getText().toString())
        .enqueue(this);
  }

  @OnClick(R.id.edit_gejala_delete_button) public void onGejalaDelete(View view) {
    mOperation = "delete";
    AdminApiService.getGejalaApiServices().deleteGejala(mGejala.id).enqueue(this);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    if (mProgressDialog != null) {
      mProgressDialog.dismiss();
    }
    Intent intent = new Intent();
    intent.putExtra("operation", mOperation);
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }
}
