package me.bagus.foodcombining.admin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Consultation;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnswerQuestionActivity extends AppCompatActivity
    implements Callback<AfterChangeCallback> {
  @BindView(R.id.answer_question_answer_field) EditText mAnswerField;
  @BindView(R.id.answer_question_question) TextView mQuestion;
  @BindView(R.id.answer_question_date) TextView mDate;
  private Consultation mConsultation;
  private MaterialDialog mProgressDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_answer_question);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    mConsultation = Parcels.unwrap(getIntent().getParcelableExtra("parcel"));

    mQuestion.setText(mConsultation.pertanyaan);
    mDate.setText(mConsultation.tanggal.date);

    if (mConsultation.jawaban != null) {
      mAnswerField.setText(mConsultation.jawaban);
    }
  }

  @OnClick(R.id.answer_question_save_button) public void onSaveButtonClicked(View view) {
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Saving your answer").build();
    mProgressDialog.show();
    AdminApiService.getKonsultasiApiServices()
        .postAnswer(mConsultation.id, mAnswerField.getText().toString())
        .enqueue(this);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    mProgressDialog.dismiss();
    setResult(RESULT_OK);

    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }
}
