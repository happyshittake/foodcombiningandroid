package me.bagus.foodcombining.admin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.admin.GejalaCheckboxListAdapter;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Gejala;
import me.bagus.foodcombining.api.admin.models.Penyebab;
import me.bagus.foodcombining.api.admin.models.PenyebabWithAturan;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPenyebabActivity extends AppCompatActivity
    implements Callback<AfterChangeCallback> {
  @BindView(R.id.edit_penyebab_nama) EditText mNamaField;
  @BindView(R.id.edit_penyebab_gejala_list) RecyclerView mRecyclerView;
  @BindView(R.id.edit_penyebab_solusi) EditText mSolusiField;
  private MaterialDialog mProgressDialog;

  private Penyebab mPenyebab;
  private String mAction;
  private ArrayList<Gejala> mGejalas;
  private GejalaCheckboxListAdapter mListAdapter;
  private Callback<Base<PenyebabWithAturan>> getAturancb =
      new Callback<Base<PenyebabWithAturan>>() {
        @Override public void onResponse(Call<Base<PenyebabWithAturan>> call,
            Response<Base<PenyebabWithAturan>> response) {
          mListAdapter = new GejalaCheckboxListAdapter(mGejalas, response.body().data.gejalas);

          mRecyclerView.setAdapter(mListAdapter);
          mRecyclerView.setLayoutManager(new LinearLayoutManager(EditPenyebabActivity.this));
        }

        @Override public void onFailure(Call<Base<PenyebabWithAturan>> call, Throwable t) {
          t.printStackTrace();
        }
      };
  private Callback<Base<ArrayList<Gejala>>> getGejalasService =
      new Callback<Base<ArrayList<Gejala>>>() {
        @Override public void onResponse(Call<Base<ArrayList<Gejala>>> call,
            Response<Base<ArrayList<Gejala>>> response) {
          mGejalas = response.body().data;

          AdminApiService.getDiagnosisApiServices()
              .getPenyebabAturan(mPenyebab.id)
              .enqueue(getAturancb);
        }

        @Override public void onFailure(Call<Base<ArrayList<Gejala>>> call, Throwable t) {
          t.printStackTrace();
        }
      };

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_penyebab);
    ButterKnife.bind(this);

    mPenyebab = Parcels.unwrap(getIntent().getParcelableExtra("parcel"));

    mNamaField.setText(mPenyebab.nama);

    mSolusiField.setText(mPenyebab.solusi);

    AdminApiService.getGejalaApiServices().getGejalas().enqueue(getGejalasService);
  }

  @OnClick(R.id.edit_penyebab_save_button) public void onSaveButtonClicked(View view) {
    mAction = "update";
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Saving...").build();
    mProgressDialog.show();
    String gejalaids = "";
    if (mListAdapter != null) {
      gejalaids = mListAdapter.getCheckedIds();
    }

    AdminApiService.getDiagnosisApiServices()
        .updatePenyebab(mPenyebab.id, mNamaField.getText().toString(), gejalaids,
            mSolusiField.getText().toString())
        .enqueue(this);
  }

  @OnClick(R.id.edit_penyebab_delete_button) public void onDeleteButtonClicked(View view) {
    mAction = "delete";
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Deleting...").build();
    mProgressDialog.show();

    AdminApiService.getDiagnosisApiServices().deletePenyebab(mPenyebab.id).enqueue(this);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    mProgressDialog.dismiss();
    Intent intent = new Intent();
    intent.putExtra("operation", mAction);
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }
}
