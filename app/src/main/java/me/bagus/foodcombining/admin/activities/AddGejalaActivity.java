package me.bagus.foodcombining.admin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGejalaActivity extends AppCompatActivity implements Callback<AfterChangeCallback> {
  @BindView(R.id.add_gejala_nama_field) EditText mNamaField;
  private MaterialDialog mProgressDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_gejala);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
  }

  @OnClick(R.id.add_gejala_save_button) public void onSaveButtonClicked(View view) {
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Saving...").build();
    mProgressDialog.show();
    AdminApiService.getGejalaApiServices()
        .storeGejala(mNamaField.getText().toString())
        .enqueue(this);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    mProgressDialog.dismiss();
    setResult(RESULT_OK);
    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }
}
