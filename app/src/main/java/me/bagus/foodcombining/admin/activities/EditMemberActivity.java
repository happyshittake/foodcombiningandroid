package me.bagus.foodcombining.admin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Member;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMemberActivity extends AppCompatActivity implements Callback<AfterChangeCallback> {
  @BindView(R.id.edit_member_delete) Button mButtonDelete;
  @BindView(R.id.edit_member_save) Button mButtonSave;
  @BindView(R.id.edit_member_field_name) EditText mFieldName;
  @BindView(R.id.edit_promote_member) Button mButtonPromote;

  private Member mMember;
  private String mOperation = "";
  private Callback<AfterChangeCallback> deleteCallback = new Callback<AfterChangeCallback>() {
    @Override
    public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
      Intent intent = new Intent();
      intent.putExtra("operation", mOperation);

      setResult(RESULT_OK, intent);

      finish();
    }

    @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
      t.printStackTrace();
    }
  };

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_member);
    ButterKnife.bind(this);

    mMember = Parcels.unwrap(getIntent().getParcelableExtra("parcel"));
    mFieldName.setText(mMember.nama);

    if (mMember.admin) {
      mButtonPromote.setText("Depromote");
    } else {
      mButtonPromote.setText("Promote");
    }
  }

  @OnClick(R.id.edit_promote_member) public void onButtonPromote(View view) {
    mOperation = "promote";
    if (mMember.admin) {
      AdminApiService.getMemberApiServices().depromoteMember(mMember.id).enqueue(deleteCallback);
    } else {
      AdminApiService.getMemberApiServices().promoteMember(mMember.id).enqueue(deleteCallback);
    }
  }

  @OnClick(R.id.edit_member_save) public void onButtonUpdateClicked(View view) {
    mOperation = "update";
    AdminApiService.getMemberApiServices()
        .updateMember(mMember.id, mFieldName.getText().toString())
        .enqueue(this);
  }

  @OnClick(R.id.edit_member_delete) public void onButtonDeleteClicked(View view) {
    mOperation = "delete";

    AdminApiService.getMemberApiServices().deleteMember(mMember.id).enqueue(deleteCallback);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    Intent intent = new Intent();

    intent.putExtra("operation", mOperation);
    setResult(RESULT_OK, intent);

    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }
}
