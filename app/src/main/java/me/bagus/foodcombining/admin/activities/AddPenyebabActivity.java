package me.bagus.foodcombining.admin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.admin.GejalaCheckboxListAdapter;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Gejala;
import me.bagus.foodcombining.api.admin.models.Penyebab;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPenyebabActivity extends AppCompatActivity
    implements Callback<AfterChangeCallback> {
  @BindView(R.id.add_penyebab_gejala_list) RecyclerView mRecyclerView;
  @BindView(R.id.add_penyebab_nama) EditText mNamaField;
  @BindView(R.id.add_penyebab_solusi) EditText mSolusiField;
  private GejalaCheckboxListAdapter mListAdapter;

  private Penyebab mPenyebab;
  private Callback<Base<ArrayList<Gejala>>> getGejalascb = new Callback<Base<ArrayList<Gejala>>>() {
    @Override public void onResponse(Call<Base<ArrayList<Gejala>>> call,
        Response<Base<ArrayList<Gejala>>> response) {
      mListAdapter = new GejalaCheckboxListAdapter(response.body().data, new ArrayList<Gejala>());
      mRecyclerView.setAdapter(mListAdapter);
      mRecyclerView.setLayoutManager(new LinearLayoutManager(AddPenyebabActivity.this));
    }

    @Override public void onFailure(Call<Base<ArrayList<Gejala>>> call, Throwable t) {
      t.printStackTrace();
    }
  };
  private MaterialDialog mProgressDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_penyebab);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    AdminApiService.getGejalaApiServices().getGejalas().enqueue(getGejalascb);
  }

  @OnClick(R.id.add_penyebab_save_button) public void onSaveButtonClicked(View view) {
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Saving ...").build();
    String gejalaIds = "";
    if (mListAdapter != null) {
      gejalaIds = mListAdapter.getCheckedIds();
    }

    AdminApiService.getDiagnosisApiServices()
        .storePenyebab(mNamaField.getText().toString(), gejalaIds,
            mSolusiField.getText().toString())
        .enqueue(this);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    mProgressDialog.dismiss();
    setResult(RESULT_OK);
    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }
}
