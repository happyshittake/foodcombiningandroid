package me.bagus.foodcombining.admin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import java.io.File;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.utils.RequestBodyFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRecipeActivity extends AppCompatActivity
    implements ImagePickerCallback, Callback<AfterChangeCallback> {
  @BindView(R.id.add_recipe_nama_field) EditText mNamaField;
  @BindView(R.id.add_recipe_deskripsi) EditText mDeskripsiField;
  @BindView(R.id.add_recipe_keterangan) EditText mKeteranganField;
  @BindView(R.id.add_recipe_fixed_checkbox) CheckBox mFixedCheckbox;
  @BindView(R.id.add_recipe_image_preview) ImageView mImagePreview;
  @BindView(R.id.add_recipe_kategori_spinner) Spinner mKategoriSpinner;
  private MaterialDialog mProgressDialog;

  private File mSelectedFile;
  private CameraImagePicker cameraPicker;
  private String pickerPath;
  private ImagePicker imagePicker;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_recipe);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    ArrayAdapter<CharSequence> adapter =
        ArrayAdapter.createFromResource(this, R.array.list_tipe_makan,
            android.R.layout.simple_spinner_item);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    mKategoriSpinner.setAdapter(adapter);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      if (requestCode == Picker.PICK_IMAGE_DEVICE) {
        if (imagePicker == null) {
          imagePicker = new ImagePicker(this);
          imagePicker.setImagePickerCallback(this);
        }
        imagePicker.submit(data);
      } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
        if (cameraPicker == null) {
          cameraPicker = new CameraImagePicker(this);
          cameraPicker.setImagePickerCallback(this);
          cameraPicker.reinitialize(pickerPath);
        }
        cameraPicker.submit(data);
      }
    }
  }

  @OnClick(R.id.add_recipe_select_image_button) public void onImagePickSelectButton(View view) {
    new MaterialDialog.Builder(this).negativeText("Pick From Gallery")
        .neutralText("Cancel")
        .onNegative(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            pickImageSingle();
            dialog.dismiss();
          }
        })
        .onNeutral(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            dialog.dismiss();
          }
        })
        .show();
  }

  private void pickImageSingle() {
    imagePicker = new ImagePicker(this);
    imagePicker.setRequestId(1234);
    imagePicker.ensureMaxSize(5090, 5090);
    imagePicker.shouldGenerateMetadata(true);
    imagePicker.shouldGenerateThumbnails(true);
    imagePicker.setImagePickerCallback(this);
    Bundle bundle = new Bundle();
    bundle.putInt("android.intent.extras.CAMERA_FACING", 1);
    imagePicker.pickImage();
  }

  private void takePicture() {
    cameraPicker = new CameraImagePicker(this);
    cameraPicker.setImagePickerCallback(this);
    cameraPicker.shouldGenerateMetadata(true);
    cameraPicker.shouldGenerateThumbnails(true);
    pickerPath = cameraPicker.pickImage();
  }

  @OnClick(R.id.add_recipe_save_button) public void onButtonSaveClicked(View view) {
    mProgressDialog =
        new MaterialDialog.Builder(this).progress(true, 0).content("Menyimpan resep...").build();
    mProgressDialog.show();
    AdminApiService.getRecipeApiServices()
        .postNewRecipe(RequestBodyFactory.createPartFromString(mNamaField.getText().toString()),
            RequestBodyFactory.createPartFromString(mKategoriSpinner.getSelectedItem().toString()),
            RequestBodyFactory.createPartFromString(mDeskripsiField.getText().toString()),
            RequestBodyFactory.createPartFromString(mKeteranganField.getText().toString()),
            RequestBodyFactory.createPartFromString(mFixedCheckbox.isChecked() ? "yes" : "no"),
            RequestBodyFactory.prepareFilePart("foto", mSelectedFile))
        .enqueue(this);
  }

  @Override
  public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
    mProgressDialog.dismiss();
    Intent intent = new Intent();
    intent.putExtra("operation", "add");

    setResult(RESULT_OK, intent);

    finish();
  }

  @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
    t.printStackTrace();
  }

  @Override public void onImagesChosen(List<ChosenImage> list) {
    for (ChosenImage image : list) {
      mSelectedFile = new File(image.getOriginalPath());

      Glide.with(this).load(mSelectedFile).crossFade().centerCrop().into(mImagePreview);
    }
  }

  @Override public void onError(String s) {
    Log.e("PICKIMAGE", s);
  }
}
