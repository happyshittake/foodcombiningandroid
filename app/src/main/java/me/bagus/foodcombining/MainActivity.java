package me.bagus.foodcombining;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.Calendar;

import me.bagus.foodcombining.adapters.AnswerHistory;
import me.bagus.foodcombining.adapters.QuizAnswerHistory;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.db.AlarmRecord;
import me.bagus.foodcombining.fragments.AddAlarmFragment;
import me.bagus.foodcombining.fragments.AddDiaryFragment;
import me.bagus.foodcombining.fragments.AddQuestionFragment;
import me.bagus.foodcombining.fragments.DiagnosisFragment;
import me.bagus.foodcombining.fragments.EatingAlarmFragment;
import me.bagus.foodcombining.fragments.FaqFragment;
import me.bagus.foodcombining.fragments.FoodDiaryFragment;
import me.bagus.foodcombining.fragments.KonsultasiFragment;
import me.bagus.foodcombining.fragments.ListGameScoreFragment;
import me.bagus.foodcombining.fragments.ListQuizScoreFragment;
import me.bagus.foodcombining.fragments.MenuDetailFragment;
import me.bagus.foodcombining.fragments.MenuFragment;
import me.bagus.foodcombining.fragments.MyMenuFragment;
import me.bagus.foodcombining.fragments.PickGameTimeFragment;
import me.bagus.foodcombining.fragments.PostsFragment;
import me.bagus.foodcombining.fragments.QuizFragment;
import me.bagus.foodcombining.fragments.QuizResultFragment;
import me.bagus.foodcombining.fragments.SwipeGameResult;
import me.bagus.foodcombining.fragments.SwipingGameFragment;
import me.bagus.foodcombining.fragments.VideoFragment;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.services.AlarmNotifier;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements Drawer.OnDrawerItemClickListener, FragmentCallback {

    private static int UPLOAD_MENU_CONST = 001;
    private static int UPDATE_MENU_CONST = 002;
    private Toolbar mToolbar;
    private FragmentManager mFragmentManager;
    private Drawer mDrawer;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("DEBUG", "MAIN ACTIVITY ON CREATE");
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mFragmentManager = getSupportFragmentManager();
        mUser = PrefUtils.getCurrentUser(this);

        PrimaryDrawerItem postItem =
                new PrimaryDrawerItem().withName("Facebook Posts").withTag("POST_MENU");
        PrimaryDrawerItem swipeGameItem =
                new PrimaryDrawerItem().withName("Swiping Game").withTag("SWIPE_GAME");
        PrimaryDrawerItem faqItem = new PrimaryDrawerItem().withName("Faq").withTag("FAQ");
        PrimaryDrawerItem videoItem = new PrimaryDrawerItem().withName("Video").withTag("VIDEO");
        PrimaryDrawerItem foodDiaryItem =
                new PrimaryDrawerItem().withName("Food Diary").withTag("FOOD_DIARY");
        PrimaryDrawerItem eatingAlarmItem =
                new PrimaryDrawerItem().withName("Eating Alarm").withTag("EATING_ALARM");
        PrimaryDrawerItem quizItem = new PrimaryDrawerItem().withName("Quiz").withTag("QUIZ");
        PrimaryDrawerItem konsultasiItem =
                new PrimaryDrawerItem().withName("Konsultasi").withTag("KONSULTASI");
        PrimaryDrawerItem menuItem = new PrimaryDrawerItem().withName("Menu").withTag("MENU");
        PrimaryDrawerItem diagnosisItem =
                new PrimaryDrawerItem().withName("Diagnosis").withTag("DIAGNOSIS");
        PrimaryDrawerItem logoutItem = new PrimaryDrawerItem().withName("Logout").withTag("LOGOUT");
        PrimaryDrawerItem myMenuItem = new PrimaryDrawerItem().withName("My Menu").withTag("MYRECIPE");
        PrimaryDrawerItem adminMenuItem =
                new PrimaryDrawerItem().withName("Admin Menu").withTag("CHANGEMODE");

        User user = PrefUtils.getCurrentUser(this);

        mDrawer = new DrawerBuilder().withActivity(this)
                .withToolbar(mToolbar)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(new AccountHeaderBuilder().withActivity(this)
                        .addProfiles(new ProfileDrawerItem().withName(user.nama).withEmail(user.email))
                        .withHeaderBackground(R.drawable.header_bg)
                        .build())
                .addDrawerItems(postItem, new DividerDrawerItem(), swipeGameItem, new DividerDrawerItem(),
                        faqItem, new DividerDrawerItem(), foodDiaryItem, new DividerDrawerItem(),
                        eatingAlarmItem, new DividerDrawerItem(), quizItem, new DividerDrawerItem(),
                        konsultasiItem, new DividerDrawerItem(), menuItem, new DividerDrawerItem(),
                        diagnosisItem, new DividerDrawerItem(), myMenuItem, new DividerDrawerItem(), logoutItem,
                        new DividerDrawerItem(), videoItem, new DividerDrawerItem())
                .withOnDrawerItemClickListener(this)
                .build();

        if (mUser.isAdmin) {
            mDrawer.addItem(adminMenuItem);
        }

        if (getIntent().getStringExtra("gotoadddiary") != null) {
            mDrawer.setSelection(foodDiaryItem, true);
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment, AddDiaryFragment.newInstance())
                    .commit();
        } else {
            mDrawer.setSelection(postItem, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == UPLOAD_MENU_CONST) {
            Toast.makeText(this, "Menu berhasil diupload", Toast.LENGTH_SHORT).show();
            onRecipeSaved();
        } else if (resultCode == RESULT_OK && requestCode == UPDATE_MENU_CONST) {
            String operation = data.getStringExtra("operation");

            if (operation.equalsIgnoreCase("update")) {
                Toast.makeText(this, "Resep berhasil diupdate", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Resep berhasil dihapus", Toast.LENGTH_SHORT).show();
            }

            onMyRecipeUpdated();
        }
    }

    private void onMyRecipeUpdated() {
        MyMenuFragment fragment = (MyMenuFragment) mFragmentManager.findFragmentByTag("MYRECIPE");

        fragment.refreshData();
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

        switch ((String) drawerItem.getTag()) {
            case "POST_MENU":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, PostsFragment.newInstance(), "POST_MENU")
                        .commit();
                break;
            case "SWIPE_GAME":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, PickGameTimeFragment.newInstance(), "SWIPE_GAME")
                        .commit();
                break;
            case "FAQ":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, FaqFragment.newInstance(), "FAQ")
                        .commit();
                break;
            case "FOOD_DIARY":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, FoodDiaryFragment.newInstance(), "FOOD_DIARY")
                        .commit();
                break;
            case "EATING_ALARM":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, EatingAlarmFragment.newInstance(), "EATING_ALARM")
                        .commit();
                break;
            case "QUIZ":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, QuizFragment.newInstance(), "QUIZ")
                        .commit();
                break;
            case "KONSULTASI":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, KonsultasiFragment.newInstance(), "KONSULTASI")
                        .commit();
                break;
            case "MENU":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, MenuFragment.newInstance(), "MENU")
                        .commit();
                break;
            case "DIAGNOSIS":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, DiagnosisFragment.newInstance(), "DIAGNOSIS")
                        .commit();
                break;
            case "MYRECIPE":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, new MyMenuFragment(), "MYRECIPE")
                        .commit();
                break;
            case "CHANGEMODE":
                Intent intent = new Intent(this, ModePickerActivity.class);
                startActivity(intent);
                finish();
                break;
            case "LOGOUT":
                PrefUtils.clearCurrentUser(this);
                Intent logoutintent = new Intent(MainActivity.this, FbLoginActivity.class);
                startActivity(logoutintent);
                finish();
                break;
            case "VIDEO":
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, new VideoFragment(), "VIDEO")
                        .commit();
                break;
        }

        return false;
    }

    @Override
    public void onGameTimePicked(String time) {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, SwipingGameFragment.newInstance(time))
                .commit();
    }

    @Override
    public void onSwipeGameEnded(final int score, final ArrayList<AnswerHistory> answers) {
        Log.d("user token", String.valueOf(mUser.token));
        Log.d("skor", String.valueOf(score));
        ApiService.getSwipeGameServices().saveScore(mUser.token, score).enqueue(new Callback<AfterChangeCallback>() {
            @Override
            public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
                String result;
                if (score > 4) {
                    result = "menang";
                } else {
                    result = "kalah";
                }

                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, ListGameScoreFragment.newInstance(result, answers))
                        .commit();
            }

            @Override
            public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onGameResultShow(String result, ArrayList<AnswerHistory> answerHistories) {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, SwipeGameResult.newInstance(result, answerHistories))
                .commit();
    }

    @Override
    public void tambahLayout() {
        mFragmentManager.beginTransaction().replace(R.id.fragment, new Fragment()).commit();
    }

    @Override
    public void onAlarmSave(String tipeAlarm, String namaMakanan, Calendar pickedDateTime) {
        int hour = pickedDateTime.get(Calendar.HOUR_OF_DAY);
        int minute = pickedDateTime.get(Calendar.MINUTE);
        AlarmRecord newAlarm = new AlarmRecord(getTipeMakanCode(tipeAlarm), namaMakanan, hour, minute);

        newAlarm.save();
        Intent intent = new Intent(this, AlarmNotifier.class);
        intent.putExtra("MESSAGE", "Alarm Makan!!! anda berencana makan " + newAlarm.getMakanan());
        intent.putExtra("alarm_id", newAlarm.getId().intValue());
        PendingIntent pintent = PendingIntent.getService(this, newAlarm.getTipe(), intent, 0);
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, pickedDateTime.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pintent);
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, EatingAlarmFragment.newInstance())
                .commit();
    }

    @Override
    public void onAddAlarmButtonClick() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, AddAlarmFragment.newInstance())
                .commit();
    }

    @Override
    public void onAddDiaryButtonClick() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, AddDiaryFragment.newInstance())
                .commit();
    }

    @Override
    public void onDiarySaved() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, FoodDiaryFragment.newInstance())
                .commit();
    }

    @Override
    public void onTambahPertanyaanClicked() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, AddQuestionFragment.newInstance())
                .commit();
    }

    @Override
    public void onPertanyaanSaved() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, KonsultasiFragment.newInstance())
                .commit();
    }

    @Override
    public void onMenuItemClicked(int id) {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, MenuDetailFragment.newInstance(id))
                .commit();
    }

    @Override
    public void onAddMenuButtonClicked() {
        Intent intent = new Intent(this, UploadMenuActivity.class);

        startActivityForResult(intent, UPLOAD_MENU_CONST);
    }

    @Override
    public void onRecipeSaved() {
        MenuFragment menuFragment = (MenuFragment) mFragmentManager.findFragmentByTag("MENU");
        menuFragment.refreshData();
    }

    @Override
    public void onMyMenuItemClicked(int id) {
        Intent intent = new Intent(this, UpdateMenuActivity.class);
        intent.putExtra("menu_id", id);
        startActivityForResult(intent, UPDATE_MENU_CONST);
    }

    @Override
    public void onQuizScoreSaved(ArrayList<QuizAnswerHistory> answerHistories) {
        mToolbar.setTitle("List Skor");
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, ListQuizScoreFragment.newInstance(answerHistories))
                .commit();
    }

    @Override
    public void onShowQuizResult(ArrayList<QuizAnswerHistory> answerHistories) {
        mToolbar.setTitle("Hasil quiz");
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment, QuizResultFragment.newInstance(answerHistories))
                .commit();
    }

    private int getTipeMakanCode(String tipeMakan) {
        if (tipeMakan.equalsIgnoreCase("siang")) {
            return 1;
        } else if (tipeMakan.equalsIgnoreCase("malam")) {
            return 2;
        } else if (tipeMakan.equalsIgnoreCase("sore")) {
            return 3;
        }

        return 0;
    }
}
