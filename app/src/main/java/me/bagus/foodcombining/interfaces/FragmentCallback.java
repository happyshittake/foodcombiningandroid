package me.bagus.foodcombining.interfaces;

import java.util.ArrayList;
import java.util.Calendar;

import me.bagus.foodcombining.adapters.AnswerHistory;
import me.bagus.foodcombining.adapters.QuizAnswerHistory;

public interface FragmentCallback {
    void onGameTimePicked(String time);

    void onSwipeGameEnded(int score, ArrayList<AnswerHistory> anwers);

    void onGameResultShow(String result, ArrayList<AnswerHistory> answerHistories);

    void tambahLayout();

    void onAlarmSave(String tipeAlarm, String namaMakanan, Calendar pickedDateTime);

    void onAddAlarmButtonClick();

    void onAddDiaryButtonClick();

    void onDiarySaved();

    void onTambahPertanyaanClicked();

    void onPertanyaanSaved();

    void onMenuItemClicked(int id);

    void onAddMenuButtonClicked();

    void onRecipeSaved();

    void onMyMenuItemClicked(int id);

    void onQuizScoreSaved(ArrayList<QuizAnswerHistory> answerHistories);

    void onShowQuizResult(ArrayList<QuizAnswerHistory> answerHistories);
}
