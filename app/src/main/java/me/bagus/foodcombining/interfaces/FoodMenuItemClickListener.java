package me.bagus.foodcombining.interfaces;

import android.view.View;

/**
 * Created by ndjoe on 22/06/16.
 */
public interface FoodMenuItemClickListener {

    public void onItemClick(View v, int position);
}
