package me.bagus.foodcombining.interfaces.admin;

import android.os.Parcelable;

public interface FragmentAction {
  void editMember(Parcelable parcelable);

  void editRecipe(Parcelable parcelable);

  void addRecipe();

  void answerConsultationQuestion(Parcelable parcelable);

  void addGejala();

  void editGejala(Parcelable parcelable);

  void addPenyebab();

  void editPenyebab(Parcelable parcelable);
}
