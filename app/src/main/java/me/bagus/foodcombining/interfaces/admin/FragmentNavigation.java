package me.bagus.foodcombining.interfaces.admin;

public interface FragmentNavigation {
    void gotoMemberMenuFragment();

    void gotoRecipeMenuFragment();

    void gotoKonsultasiMenuFragment();

    void gotoGejalaMenuFragment();

    void gotoAturanFragment();

    void gotoVideoFragment();

    void gotoFaqFragment();
}
