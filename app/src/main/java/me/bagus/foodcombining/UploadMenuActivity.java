package me.bagus.foodcombining;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import java.io.File;
import java.util.List;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Response;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.RequestBodyFactory;
import me.bagus.foodcombining.utils.User;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class UploadMenuActivity extends AppCompatActivity
    implements Callback<Response>, ImagePickerCallback {
  private Button mSaveButton;
  private EditText mNameField;
  private EditText mDeskripsiField;
  private EditText mKeteranganField;
  private Spinner mKategoriField;
  private View mRootView;
  private Button mPickPhotoButton;
  private ImageView mImagePreview;
  private File mSelectedFile;

  private static int REQUEST_CAMERA = 900;
  private static int SELECT_FILE = 901;
  private ImagePicker imagePicker;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_upload_menu);

    mRootView = findViewById(android.R.id.content);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    Window window = getWindow();
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      window.setStatusBarColor(getResources().getColor(R.color.primary));
    }

    initWidgets();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();

        return (true);
    }

    return super.onOptionsItemSelected(item);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      if (requestCode == Picker.PICK_IMAGE_DEVICE) {
        if (imagePicker == null) {
          imagePicker = new ImagePicker(this);
          imagePicker.setImagePickerCallback(this);
        }
        imagePicker.submit(data);
      }
    }
  }

  private void initWidgets() {
    mSaveButton = (Button) findViewById(R.id.activity_add_menu_save_button);
    mDeskripsiField = (EditText) findViewById(R.id.activity_add_menu_deskripsi);
    mKategoriField = (Spinner) findViewById(R.id.activity_add_menu_kategori);
    mKeteranganField = (EditText) findViewById(R.id.activity_add_menu_keterangan);
    mNameField = (EditText) findViewById(R.id.activity_add_menu_nama);
    mImagePreview = (ImageView) findViewById(R.id.activity_add_menu_image_viewer);
    mPickPhotoButton = (Button) findViewById(R.id.activity_add_menu_pick_photo_button);

    mPickPhotoButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        selectImage();
      }
    });

    ArrayAdapter<CharSequence> adapter =
        ArrayAdapter.createFromResource(this, R.array.list_tipe_makan,
            android.R.layout.simple_spinner_item);

    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    mKategoriField.setAdapter(adapter);

    final User user = PrefUtils.getCurrentUser(this);

    mSaveButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        String name = mNameField.getText().toString();
        String deskripsi = mDeskripsiField.getText().toString();
        String kategori = mKategoriField.getSelectedItem().toString();
        String keterangan = mKeteranganField.getText().toString();
        int id = user.token;

        RequestBody reqName = RequestBody.create(MediaType.parse("multipart/form-data"), name);
        RequestBody reqDeksripsi =
            RequestBody.create(MediaType.parse("multipart/form-data"), deskripsi);
        RequestBody reqKategori =
            RequestBody.create(MediaType.parse("multipart/form-data"), kategori);
        RequestBody reqKeterangan =
            RequestBody.create(MediaType.parse("multipart/form-data"), keterangan);
        RequestBody reqToken =
            RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(id));

        ApiService.getFoodMenuServices()
            .postRecipe(reqName, reqDeksripsi, reqKategori, reqKeterangan, reqToken,
                RequestBodyFactory.prepareFilePart("foto", mSelectedFile))
            .enqueue(UploadMenuActivity.this);
      }
    });
  }

  @Override public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
    setResult(RESULT_OK);
    finish();
  }

  @Override public void onFailure(Call<Response> call, Throwable t) {
    Snackbar.make(mRootView, "upload menu failed", Snackbar.LENGTH_SHORT);
    t.printStackTrace();
  }

  private void selectImage() {
    final CharSequence[] items = {
        "Choose from Library", "Cancel"
    };
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Add Photo!");
    builder.setItems(items, new DialogInterface.OnClickListener() {
      @Override public void onClick(DialogInterface dialog, int item) {
        if (items[item].equals("Choose from Library")) {
          pickImage();
          dialog.dismiss();
        } else if (items[item].equals("Cancel")) {
          dialog.dismiss();
        }
      }
    });
    builder.show();
  }

  private void pickImage() {
    imagePicker = new ImagePicker(this);
    imagePicker.setRequestId(1234);
    imagePicker.ensureMaxSize(5090, 5090);
    imagePicker.shouldGenerateMetadata(true);
    imagePicker.shouldGenerateThumbnails(true);
    imagePicker.setImagePickerCallback(this);
    Bundle bundle = new Bundle();
    bundle.putInt("android.intent.extras.CAMERA_FACING", 1);
    imagePicker.pickImage();
  }

  @Override public void onImagesChosen(List<ChosenImage> list) {
    for (ChosenImage image : list) {
      mSelectedFile = new File(image.getOriginalPath());

      Glide.with(this).load(mSelectedFile).crossFade().centerCrop().into(mImagePreview);
    }
  }

  @Override public void onError(String s) {
    Log.e("error", s);
  }
}
