package me.bagus.foodcombining.utils;


public class User {
    public String nama;
    public int token;
    public String email;
    public boolean isAdmin;

    @Override
    public String toString() {
        return "User{" +
                "nama='" + nama + '\'' +
                ", token=" + token +
                ", email='" + email + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
