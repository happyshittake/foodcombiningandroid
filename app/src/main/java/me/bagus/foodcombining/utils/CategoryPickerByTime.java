package me.bagus.foodcombining.utils;

public class CategoryPickerByTime {
  public static String pick(int hour, int minute) {
    double clock = hour + (minute / 60);
    if (clock > 4 && clock < 12) {
      return "PAGI";
    } else if (clock >= 12 && clock < 14) {
      return "SIANG";
    } else if (clock >= 12 && clock < 17.5) {
      return "SORE";
    } else {
      return "MALAM";
    }
  }
}
