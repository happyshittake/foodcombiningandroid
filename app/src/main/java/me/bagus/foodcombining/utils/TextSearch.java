package me.bagus.foodcombining.utils;

import java.util.ArrayList;
import me.bagus.foodcombining.api.models.Faq;

public class TextSearch {
  private ArrayList<Faq> mFaqs;

  public TextSearch(ArrayList<Faq> faqs) {
    mFaqs = faqs;
  }

  public ArrayList<Faq> fiter(CharSequence s) {
    String filterSeq = s.toString().toLowerCase();
    if (filterSeq != null && filterSeq.length() > 0) {
      ArrayList<Faq> filter = new ArrayList<>();

      for (Faq faq : mFaqs) {
        if (faq.pertanyaan.toLowerCase().contains(filterSeq)) {
          filter.add(faq);
        }
      }

      return filter;
    }

    return mFaqs;
  }
}
