package me.bagus.foodcombining.utils;

import android.support.annotation.NonNull;
import java.io.File;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RequestBodyFactory {
  public static final String MULTIPART_FORM_DATA = "multipart/form-data";

  @NonNull public static RequestBody createPartFromString(String descriptionString) {
    return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
  }

  public static MultipartBody.Part prepareFilePart(String partName, File file) {

    if (file == null) {
      return null;
    }
    // create RequestBody instance from file
    RequestBody requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);

    // MultipartBody.Part is used to send also the actual file name
    return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
  }
}
