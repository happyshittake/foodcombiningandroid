package me.bagus.foodcombining.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.bagus.foodcombining.DeveloperKey;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.VideoEntry;
import me.bagus.foodcombining.adapters.YTVideoAdapter;
import me.bagus.foodcombining.api.admin.AdminApi;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Video;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoFragment extends Fragment implements Callback<Base<ArrayList<Video>>> {
  @BindView(R.id.video_recycler) RecyclerView mRecyclerView;
  private AdminApi.VideoApi videoApi;

  public VideoFragment() {
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_list_video, container, false);
    ButterKnife.bind(this, view);
    videoApi = AdminApiService.getVideoApiServices();

    return view;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    videoApi.getVideos().enqueue(this);
  }

  private ArrayList<VideoEntry> mapVideos(ArrayList<Video> videos) {
    ArrayList<VideoEntry> entries = new ArrayList<>();
    for (Video video : videos) {
      entries.add(new VideoEntry(video.url));
    }

    return entries;
  }

  @Override public void onResponse(Call<Base<ArrayList<Video>>> call,
      Response<Base<ArrayList<Video>>> response) {
    final YTVideoAdapter adapter = new YTVideoAdapter(mapVideos(response.body().data));
    mRecyclerView.setAdapter(adapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    adapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            VideoEntry entry = adapter.getItem(i);
            Intent intent =
                YouTubeStandalonePlayer.createVideoIntent(getActivity(), DeveloperKey.DEVELOPER_KEY,
                    entry.videoId);
            getActivity().startActivity(intent);
          }
        });
  }

  @Override public void onFailure(Call<Base<ArrayList<Video>>> call, Throwable t) {

  }
}
