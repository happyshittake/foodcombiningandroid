package me.bagus.foodcombining.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Response;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddQuestionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddQuestionFragment extends Fragment implements Callback<Response> {
    private View mView;

    public AddQuestionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddQuestionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddQuestionFragment newInstance() {
        AddQuestionFragment fragment = new AddQuestionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_add_question, container, false);
        final EditText questionText = (EditText) mView.findViewById(R.id.quetion_text);
        Button button = (Button) mView.findViewById(R.id.save_pertanyaan);
        final User user = PrefUtils.getCurrentUser(getContext());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiService.getKonsultasiServices()
                        .postQuestion(questionText.getText().toString(), user.token)
                        .enqueue(AddQuestionFragment.this);
            }
        });

        return mView;
    }

    @Override
    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
        FragmentCallback callback = (FragmentCallback) getContext();
        callback.onPertanyaanSaved();
    }

    @Override
    public void onFailure(Call<Response> call, Throwable t) {

    }
}
