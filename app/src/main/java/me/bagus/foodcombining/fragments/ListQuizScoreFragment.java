package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.QuizAnswerHistory;
import me.bagus.foodcombining.adapters.QuizScoreAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.QuizScore;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListQuizScoreFragment extends Fragment
    implements Callback<Base<ArrayList<QuizScore>>> {

  @BindView(R.id.fragment_list_quiz_score_recylcer) RecyclerView mRecyclerView;
  private ArrayList<QuizAnswerHistory> mQuizAnswerHistories;

  public ListQuizScoreFragment() {
    // Required empty public constructor
  }

  public static ListQuizScoreFragment newInstance(ArrayList<QuizAnswerHistory> answerHistories) {
    ListQuizScoreFragment fragment = new ListQuizScoreFragment();
    Bundle args = new Bundle();
    args.putParcelable("answer", Parcels.wrap(answerHistories));
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mQuizAnswerHistories = Parcels.unwrap(getArguments().getParcelable("answer"));
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_list_quiz_score, container, false);
    ButterKnife.bind(this, view);

    ApiService.getQuizServices().getAllScores().enqueue(this);
    return view;
  }

  @Override public void onResponse(Call<Base<ArrayList<QuizScore>>> call,
      Response<Base<ArrayList<QuizScore>>> response) {
    QuizScoreAdapter adapter = new QuizScoreAdapter(response.body().data);
    mRecyclerView.setAdapter(adapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<QuizScore>>> call, Throwable t) {

  }

  @OnClick(R.id.check_quiz_result_button) void onClick(View v) {
    FragmentCallback callback = (FragmentCallback) getActivity();
    callback.onShowQuizResult(mQuizAnswerHistories);
  }
}
