package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Response;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.RequestBodyFactory;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddMenuFragment extends Fragment implements Callback<Response> {

  private Button mSaveButton;
  private EditText mNameField;
  private EditText mDeskripsiField;
  private EditText mKeteranganField;
  private Spinner mKategoriField;
  private View mView;

  public AddMenuFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment AddMenuFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static AddMenuFragment newInstance() {
    AddMenuFragment fragment = new AddMenuFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(final LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_add_menu, container, false);
    mDeskripsiField = (EditText) mView.findViewById(R.id.fragment_add_menu_deskripsi);
    mKategoriField = (Spinner) mView.findViewById(R.id.fragment_add_menu_kategori);
    mKeteranganField = (EditText) mView.findViewById(R.id.fragment_add_menu_keterangan);
    mNameField = (EditText) mView.findViewById(R.id.fragment_add_menu_nama);
    mSaveButton = (Button) mView.findViewById(R.id.fragment_add_menu_save_button);

    ArrayAdapter<CharSequence> adapter =
        ArrayAdapter.createFromResource(getContext(), R.array.list_tipe_makan,
            android.R.layout.simple_spinner_item);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    mKategoriField.setAdapter(adapter);
    final User user = PrefUtils.getCurrentUser(getContext());

    mSaveButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        String name = mNameField.getText().toString();
        String deskripsi = mDeskripsiField.getText().toString();
        String kategori = mKategoriField.getSelectedItem().toString();
        String keterangan = mKeteranganField.getText().toString();
        int id = user.token;

        ApiService.getFoodMenuServices()
            .postRecipe(RequestBodyFactory.createPartFromString(name),
                RequestBodyFactory.createPartFromString(deskripsi),
                RequestBodyFactory.createPartFromString(kategori),
                RequestBodyFactory.createPartFromString(keterangan),
                RequestBodyFactory.createPartFromString(String.valueOf(id)),
                RequestBodyFactory.prepareFilePart("foto", null))
            .enqueue(AddMenuFragment.this);
      }
    });

    return mView;
  }

  @Override public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
    FragmentCallback callback = (FragmentCallback) getActivity();
    Snackbar.make(mView, "Recipe Saved", Snackbar.LENGTH_SHORT).show();

    callback.onRecipeSaved();
  }

  @Override public void onFailure(Call<Response> call, Throwable t) {
    t.printStackTrace();
  }
}
