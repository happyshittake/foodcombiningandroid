package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.philliphsu.bottomsheetpickers.date.BottomSheetDatePickerDialog;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.numberpad.NumberPadTimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.MenuRecipe;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.CategoryPickerByTime;
import me.bagus.foodcombining.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddAlarmFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddAlarmFragment extends Fragment
        implements View.OnClickListener, BottomSheetTimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private Button mSaveAlarmButton;
    private Button pickTimeButton;
    private Button pickDateButton;
    private NumberPadTimePickerDialog timePickerDialog;
    private BottomSheetDatePickerDialog datePickerDialog;
    private Calendar pickedDate;
    private Calendar pickedTime;
    private View mView;
    private Context mCtx;
    private Spinner foodList;
    private String mTipeAlarm;
    private int mAlarmHour;
    private int mAlarmMinute;
    private Calendar mCal;
    private EditText mMakananField;

    public AddAlarmFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddAlarmFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddAlarmFragment newInstance() {
        AddAlarmFragment fragment = new AddAlarmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCal = Calendar.getInstance();
        mAlarmHour = mCal.get(Calendar.HOUR_OF_DAY);
        mAlarmMinute = mCal.get(Calendar.MINUTE);

        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_add_alarm, container, false);
        mCtx = getContext();
        mSaveAlarmButton = (Button) mView.findViewById(R.id.simpan_alarm_button);
        pickDateButton = (Button) mView.findViewById(R.id.date_pick_button);
        foodList = (Spinner) mView.findViewById(R.id.list_makanan);
        setUpFoodList(foodList);
        pickTimeButton = (Button) mView.findViewById(R.id.time_pick_button);
        timePickerDialog = NumberPadTimePickerDialog.newInstance(this);
        Calendar now = Calendar.getInstance();
        datePickerDialog = BottomSheetDatePickerDialog.newInstance(this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        pickDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(getActivity().getSupportFragmentManager(), "BOTTOMSHEETDATEPICKER");
            }
        });
        pickTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.show(getActivity().getSupportFragmentManager(), "BOTTOMSHEETTIMEPICKER");
            }
        });

        mSaveAlarmButton.setOnClickListener(this);
        return mView;
    }

    private void setUpFoodList(final Spinner foodSpinner) {
        ApiService.getFoodMenuServices()
                .getFoodMenu(PrefUtils.getCurrentUser(getContext()).token)
                .enqueue(new Callback<Base<ArrayList<MenuRecipe>>>() {
                    @Override
                    public void onResponse(Call<Base<ArrayList<MenuRecipe>>> call,
                                           Response<Base<ArrayList<MenuRecipe>>> response) {
                        ArrayAdapter<CharSequence> adapter =
                                new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        for (MenuRecipe recipe : response.body().data) {
                            adapter.add(recipe.nama);
                        }
                        foodSpinner.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Base<ArrayList<MenuRecipe>>> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        FragmentCallback mCallback = (FragmentCallback) mCtx;
        String namaMakanan = foodList.getSelectedItem().toString();

        if (pickedDate != null &&
                pickedTime != null &&
                namaMakanan.length() > 0) {
            int hour = pickedTime.get(Calendar.HOUR_OF_DAY);
            int minute = pickedTime.get(Calendar.MINUTE);
            pickedDate.set(Calendar.HOUR_OF_DAY, hour);
            pickedDate.set(Calendar.MINUTE, minute);
            mTipeAlarm = CategoryPickerByTime.pick(hour, minute);
            mCallback.onAlarmSave(mTipeAlarm, namaMakanan, pickedDate);
        } else {
            Snackbar.make(mView, "mohon isi semua form dengan benar", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        pickedTime = cal;
        pickTimeButton.setText(DateFormat.getTimeFormat(mCtx).format(cal.getTime()));
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        pickedDate = cal;
        pickDateButton.setText(DateFormat.getDateFormat(mCtx).format(cal.getTime()));
    }
}
