package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chad.library.adapter.base.BaseQuickAdapter;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.FoodMenuAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.MenuRecipe;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyMenuFragment extends Fragment implements Callback<Base<ArrayList<MenuRecipe>>> {

  private View mView;
  @BindView(R.id.my_menu_list) RecyclerView mRecyclerView;
  private FragmentCallback mCallback;

  public MyMenuFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_my_menu, container, false);
    ButterKnife.bind(this, mView);

    mCallback = (FragmentCallback) getActivity();
    refreshData();

    return mView;
  }

  public void refreshData() {
    User user = PrefUtils.getCurrentUser(getContext());
    ApiService.getFoodMenuServices().getMyRecipes(user.token).enqueue(this);
  }

  @Override public void onResponse(Call<Base<ArrayList<MenuRecipe>>> call,
      Response<Base<ArrayList<MenuRecipe>>> response) {
    final FoodMenuAdapter adapter = new FoodMenuAdapter(response.body().data);
    adapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            mCallback.onMyMenuItemClicked(adapter.getItem(i).id);
          }
        });

    mRecyclerView.setAdapter(adapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<MenuRecipe>>> call, Throwable t) {
    t.printStackTrace();
  }
}
