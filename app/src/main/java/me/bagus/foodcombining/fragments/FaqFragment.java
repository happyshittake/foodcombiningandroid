package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.FaqAdapter;
import me.bagus.foodcombining.adapters.SectionWrapper;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.Faq;
import me.bagus.foodcombining.utils.TextSearch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FaqFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FaqFragment extends Fragment implements Callback<Base<ArrayList<Faq>>> {
  private Context mContext;
  private TextToSpeech mTts;
  private RecyclerView mFaqRecycler;
  private TextSearch mTextSearch;
  private View mView;
  private FaqAdapter mFaqAdapter;
  private EditText mSearchField;
  private ImageButton mSearchButton;

  public FaqFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment FaqFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static FaqFragment newInstance() {
    FaqFragment fragment = new FaqFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mContext = getContext();
    mTts = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
      @Override public void onInit(int i) {

      }
    });
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_faq, container, false);
    mFaqRecycler = (RecyclerView) mView.findViewById(R.id.faq_list);
    mSearchField = (EditText) mView.findViewById(R.id.faq_search_field);
    mSearchButton = (ImageButton) mView.findViewById(R.id.faq_search_button);

    mSearchButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        if (mTextSearch != null && mFaqAdapter != null) {
          ArrayList<Faq> faqs = mTextSearch.fiter(mSearchField.getText().toString());
          mFaqAdapter.setNewData(new SectionWrapper(faqs).getSections());
        }
      }
    });

    ApiService.getFaqServices().getFaqs().enqueue(this);

    return mView;
  }

  @Override
  public void onResponse(Call<Base<ArrayList<Faq>>> call, Response<Base<ArrayList<Faq>>> response) {
    mFaqAdapter = new FaqAdapter(response.body().data, mTts);
    mTextSearch = new TextSearch(response.body().data);
    mFaqRecycler.setAdapter(mFaqAdapter);
    mFaqRecycler.setLayoutManager(new LinearLayoutManager(mContext));
  }

  @Override public void onFailure(Call<Base<ArrayList<Faq>>> call, Throwable t) {
    t.printStackTrace();
    Snackbar.make(mView, "failed to fetch faqs", Snackbar.LENGTH_SHORT).show();
  }

  @Override public void onDestroy() {
    if (mTts.isSpeaking()) {
      mTts.stop();
    }

    if (mTts != null) {
      mTts.shutdown();
      mTts = null;
    }

    super.onDestroy();
  }
}
