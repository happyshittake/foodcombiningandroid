package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.AnswerHistory;
import me.bagus.foodcombining.adapters.GameResultAdapter;
import org.parceler.Parcels;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SwipeGameResult#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SwipeGameResult extends Fragment {
  private static final String GAME_RESULT = "gameResult";
  private String gameResult;
  private ArrayList<AnswerHistory> mAnswerResult;

  public SwipeGameResult() {
    // Required empty public constructor
  }

  public static SwipeGameResult newInstance(String result, ArrayList<AnswerHistory> answers) {
    SwipeGameResult fragment = new SwipeGameResult();
    Bundle args = new Bundle();
    args.putString(GAME_RESULT, result);
    args.putParcelable("answer", Parcels.wrap(answers));
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      gameResult = getArguments().getString(GAME_RESULT);
      mAnswerResult = Parcels.unwrap(getArguments().getParcelable("answer"));
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_swipe_game_result, container, false);
    TextView textView = (TextView) view.findViewById(R.id.game_result_text);
    RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.game_result_list);

    if (gameResult.equalsIgnoreCase("menang")) {
      textView.setText("Selamat anda menang");
    } else {
      textView.setText(":{ anda kalah");
    }
    GameResultAdapter adapter = new GameResultAdapter(mAnswerResult);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setAdapter(adapter);
    return view;
  }
}
