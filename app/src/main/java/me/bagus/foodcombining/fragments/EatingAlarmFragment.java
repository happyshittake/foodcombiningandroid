package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.EatingAlarmAdapter;
import me.bagus.foodcombining.db.AlarmRecord;
import me.bagus.foodcombining.interfaces.FragmentCallback;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EatingAlarmFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EatingAlarmFragment extends Fragment {

  private View mView;
  private Context mCtx;
  private RecyclerView mAlarmRecycler;
  private FloatingActionButton mAddAlarmButton;

  public EatingAlarmFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment EatingAlarmFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static EatingAlarmFragment newInstance() {
    EatingAlarmFragment fragment = new EatingAlarmFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mCtx = getContext();
    final FragmentCallback cb = (FragmentCallback) mCtx;
    mView = inflater.inflate(R.layout.fragment_eating_alarm, container, false);
    List<AlarmRecord> alarmRecords = AlarmRecord.listAll(AlarmRecord.class);
    mAlarmRecycler = (RecyclerView) mView.findViewById(R.id.alarm_list);
    mAddAlarmButton = (FloatingActionButton) mView.findViewById(R.id.add_alarm);

    EatingAlarmAdapter adapter = new EatingAlarmAdapter(alarmRecords);
    mAlarmRecycler.setAdapter(adapter);
    mAlarmRecycler.setLayoutManager(new LinearLayoutManager(mCtx));
    mAddAlarmButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        cb.onAddAlarmButtonClick();
      }
    });

    return mView;
  }
}
