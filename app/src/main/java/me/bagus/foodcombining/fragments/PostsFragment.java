package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.FbPostAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostsFragment extends Fragment implements Callback<Base<ArrayList<Post>>> {

  private View mView;
  private Context mContext;
  private RecyclerView mCardRecyclerView;
  private EditText mSearchField;
  private String mSearchString;
  private ImageButton mButtonSearch;

  public PostsFragment() {
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment PostsFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static PostsFragment newInstance() {
    PostsFragment fragment = new PostsFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d("DEBUG", "POST FRAGMENT ON CREATE");
    if (getArguments() != null) {
    }
    mSearchString = "";
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_posts, container, false);
    mContext = getContext();
    mCardRecyclerView = (RecyclerView) mView.findViewById(R.id.posts_recycler_view);
    mSearchField = (EditText) mView.findViewById(R.id.fragment_posts_search_field);
    mButtonSearch = (ImageButton) mView.findViewById(R.id.fragment_posts_search_button);

    ApiService.getPostServices().getPosts(mSearchString).enqueue(this);
    Log.i("hello", "hello from create view");

    mButtonSearch.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        mSearchString = mSearchField.getText().toString();
        ApiService.getPostServices().getPosts(mSearchString).enqueue(PostsFragment.this);
      }
    });
    return mView;
  }

  @Override public void onResponse(Call<Base<ArrayList<Post>>> call,
      Response<Base<ArrayList<Post>>> response) {
    Log.d("DEBUG", "RESPONDED REQUEST");
    FbPostAdapter adapter = new FbPostAdapter(response.body().data);
    mCardRecyclerView.setHasFixedSize(false);
    mCardRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));

    mCardRecyclerView.setAdapter(adapter);
  }

  @Override public void onFailure(Call<Base<ArrayList<Post>>> call, Throwable t) {
    t.printStackTrace();
    Snackbar.make(mView, "failed to fetch posts", Snackbar.LENGTH_SHORT).show();
  }
}
