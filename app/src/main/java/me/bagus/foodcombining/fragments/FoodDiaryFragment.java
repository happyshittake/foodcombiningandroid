package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import me.bagus.foodcombining.MainActivity;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.DiaryAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.EventDiary;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FoodDiaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FoodDiaryFragment extends Fragment implements Callback<Base<ArrayList<EventDiary>>>,
    CompactCalendarView.CompactCalendarViewListener {

  private View mView;
  private CompactCalendarView mCalendar;
  private RecyclerView mEventDiaryList;
  private ActionBar mActionBar;
  private Calendar mCal;
  private Context mCtx;
  private User mUser;
  private FragmentCallback mCallback;
  private DiaryAdapter mAdapter;
  private SimpleDateFormat mFormat;

  public FoodDiaryFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment FoodDiaryFragment.
   */
  public static FoodDiaryFragment newInstance() {
    FoodDiaryFragment fragment = new FoodDiaryFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mCal = Calendar.getInstance();
    mFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mView = inflater.inflate(R.layout.fragment_food_diary, container, false);
    mCalendar = (CompactCalendarView) mView.findViewById(R.id.diary_calendar);
    FloatingActionButton floatingActionButton =
        (FloatingActionButton) mView.findViewById(R.id.floating_add_diary_button);
    mCtx = getContext();
    mCallback = (FragmentCallback) mCtx;
    mUser = PrefUtils.getCurrentUser(mCtx);
    mActionBar = ((MainActivity) getActivity()).getSupportActionBar();
    mCalendar.setListener(this);
    mEventDiaryList = (RecyclerView) mView.findViewById(R.id.diary_list);
    setActionBarTitleToMonth(mCal.getTime());
    String timestamp = mFormat.format(mCal.getTime());

    ApiService.getDiaryServices().getEvents(timestamp, mUser.token).enqueue(this);

    floatingActionButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        mCallback.onAddDiaryButtonClick();
      }
    });

    return mView;
  }

  private void getDayEvents(String timestamp) {
    ApiService.getDiaryServices()
        .getEventsByDay(timestamp, mUser.token)
        .enqueue(new Callback<Base<ArrayList<EventDiary>>>() {
          @Override public void onResponse(Call<Base<ArrayList<EventDiary>>> call,
              Response<Base<ArrayList<EventDiary>>> response) {
            Log.d("DEBUGDATA", response.body().data.toString());
            if (mAdapter != null) {
              mAdapter.swap(response.body().data);
            } else {
              mAdapter = new DiaryAdapter(response.body().data);
              mEventDiaryList.setAdapter(mAdapter);
              mEventDiaryList.setLayoutManager(new LinearLayoutManager(mCtx));
              mAdapter.notifyDataSetChanged();
            }
          }

          @Override public void onFailure(Call<Base<ArrayList<EventDiary>>> call, Throwable t) {

          }
        });
  }

  @Override public void onResponse(Call<Base<ArrayList<EventDiary>>> call,
      Response<Base<ArrayList<EventDiary>>> response) {
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd H:m:s");
    Log.i("resp", response.body().data.toString());
    List<Event> calendarEvents = new ArrayList<>();
    Event oldEvent = null;
    for (EventDiary event : response.body().data) {
      try {
        Event calEvent = new Event(Color.RED, formatter.parse(event.time).getTime());
        if (oldEvent != null && calEvent.getTimeInMillis() != oldEvent.getTimeInMillis()) {
          calendarEvents.add(calEvent);
        }
        oldEvent = calEvent;
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }

    mCalendar.removeAllEvents();
    mCalendar.addEvents(calendarEvents);
  }

  @Override public void onFailure(Call<Base<ArrayList<EventDiary>>> call, Throwable t) {

  }

  @Override public void onDayClick(Date dateClicked) {
    getDayEvents(mFormat.format(dateClicked));
  }

  @Override public void onDestroyView() {
    mActionBar.setTitle("Food Combining");
    super.onDestroyView();
  }

  @Override public void onMonthScroll(Date firstDayOfNewMonth) {
    setActionBarTitleToMonth(firstDayOfNewMonth);
    ApiService.getDiaryServices()
        .getEvents(mFormat.format(firstDayOfNewMonth), mUser.token)
        .enqueue(this);
  }

  private void setActionBarTitleToMonth(Date date) {
    mActionBar.setTitle(new SimpleDateFormat("MMMM").format(date));
  }
}
