package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.github.dkharrat.nexusdialog.FormController;
import com.github.dkharrat.nexusdialog.FormModel;
import com.github.dkharrat.nexusdialog.controllers.FormSectionController;
import com.github.dkharrat.nexusdialog.controllers.SelectionController;
import java.util.ArrayList;
import java.util.Arrays;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.QuizAnswerHistory;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.Quiz;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link QuizFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizFragment extends Fragment
    implements Callback<Base<ArrayList<Quiz>>>, View.OnClickListener {

  private View mView;
  private Context mCtx;
  private ViewGroup mFormContainer;
  private ArrayList<Quiz> mListQuiz;
  private Button mSubmitForm;
  private FormController mFormController;
  private FragmentCallback mFragmentCallback;
  private int mScore;
  private ArrayList<QuizAnswerHistory> mAnswerHistories = new ArrayList<>();

  public QuizFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment QuizFragment.
   */
  public static QuizFragment newInstance() {
    QuizFragment fragment = new QuizFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mScore = 0;
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mView = inflater.inflate(R.layout.fragment_quiz, container, false);
    mCtx = getContext();
    mFormContainer = (ViewGroup) mView.findViewById(R.id.form_elements_container);
    mSubmitForm = (Button) mView.findViewById(R.id.submit_form);
    mFragmentCallback = (FragmentCallback) getContext();

    ApiService.getQuizServices().getQuizzes().enqueue(this);

    mSubmitForm.setOnClickListener(this);

    return mView;
  }

  @Override public void onResponse(Call<Base<ArrayList<Quiz>>> call,
      Response<Base<ArrayList<Quiz>>> response) {
    mFormController = new FormController(mCtx);

    FormSectionController sectionController = new FormSectionController(mCtx, "quiz");

    mListQuiz = response.body().data;

    for (Quiz q : mListQuiz) {
      sectionController.addElement(
          new SelectionController(mCtx, "question-" + q.id, q.pertanyaan, true, "pick choice here",
              Arrays.asList(q.list_jawaban), Arrays.asList(0, 1, 2, 3, 4)));
    }

    mFormController.addSection(sectionController);

    mFormController.recreateViews(mFormContainer);
  }

  @Override public void onFailure(Call<Base<ArrayList<Quiz>>> call, Throwable t) {
    Snackbar.make(mView, "failed to get quizzes", Snackbar.LENGTH_SHORT).show();
  }

  @Override public void onClick(View view) {
    if (mFormController != null) {
      FormModel model = mFormController.getModel();
      int tempScore = 0;
      int countAnswered = 0;
      for (Quiz q : mListQuiz) {
        if (model.getValue("question-" + q.id) != null) {
          countAnswered += 1;
          mAnswerHistories.add(new QuizAnswerHistory(q.list_jawaban, q.idx_jawaban,
              (Integer) model.getValue("question-" + q.id)));
          if ((int) model.getValue("question-" + q.id) == q.idx_jawaban) {
            tempScore += 1;
          }
        }
      }

      if (countAnswered != mFormController.getNumberOfElements()) {
        Snackbar.make(mView, "Tolong isi semua pertannyaan", Snackbar.LENGTH_SHORT).show();
      } else {
        User user = PrefUtils.getCurrentUser(getContext());
        ApiService.getQuizServices()
            .saveScore(user.token, tempScore)
            .enqueue(new Callback<me.bagus.foodcombining.api.models.Response>() {
              @Override
              public void onResponse(Call<me.bagus.foodcombining.api.models.Response> call,
                  Response<me.bagus.foodcombining.api.models.Response> response) {
                mFragmentCallback.onQuizScoreSaved(mAnswerHistories);
              }

              @Override public void onFailure(Call<me.bagus.foodcombining.api.models.Response> call,
                  Throwable t) {
                t.printStackTrace();
              }
            });
      }
    }
  }
}
