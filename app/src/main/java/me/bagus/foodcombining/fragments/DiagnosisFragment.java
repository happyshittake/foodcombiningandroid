package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.afollestad.materialdialogs.MaterialDialog;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.GejalaAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.Gejala;
import me.bagus.foodcombining.api.models.ResultDiagnosis;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DiagnosisFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DiagnosisFragment extends Fragment implements Callback<Base<ArrayList<Gejala>>> {

  private View mView;
  private GejalaAdapter mAdapter;
  private RecyclerView mGejalaList;
  private Button mDiagnosisButton;
  private Callback<ResultDiagnosis> diagnosisListener = new Callback<ResultDiagnosis>() {
    @Override
    public void onResponse(Call<ResultDiagnosis> call, Response<ResultDiagnosis> response) {
      ResultDiagnosis diagnosis = response.body();

      if (diagnosis.found) {
        new MaterialDialog.Builder(getContext()).content("Anda mengalami "
            + diagnosis.penyebab.nama
            + "\n"
            + "Solusi: "
            + diagnosis.penyebab.solusi).show();
      } else {
        Snackbar.make(mView, "Penyebab tidak ditemukan", Snackbar.LENGTH_SHORT).show();
      }
    }

    @Override public void onFailure(Call<ResultDiagnosis> call, Throwable t) {
      t.printStackTrace();
    }
  };

  public DiagnosisFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment DiagnosisFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static DiagnosisFragment newInstance() {
    DiagnosisFragment fragment = new DiagnosisFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_diagnosis, container, false);
    mDiagnosisButton = (Button) mView.findViewById(R.id.fragment_diagnosis_diagnosis_button);
    mGejalaList = (RecyclerView) mView.findViewById(R.id.fragment_diagnosis_list_gejala);

    ApiService.getDiagnosisInteface().getGejalas().enqueue(this);

    mDiagnosisButton.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        String idxs = mAdapter.getCheckedIdxsString();
        Log.d("DEBUG", idxs);
        ApiService.getDiagnosisInteface().getDiagnosis(idxs).enqueue(diagnosisListener);
      }
    });
    return mView;
  }

  @Override public void onResponse(Call<Base<ArrayList<Gejala>>> call,
      Response<Base<ArrayList<Gejala>>> response) {
    mAdapter = new GejalaAdapter(response.body().data);
    mGejalaList.setAdapter(mAdapter);
    mGejalaList.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<Gejala>>> call, Throwable t) {
    t.printStackTrace();
  }
}
