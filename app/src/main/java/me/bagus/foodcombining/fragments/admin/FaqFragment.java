package me.bagus.foodcombining.fragments.admin;

import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.FaqAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.FaqCategory;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.Faq;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaqFragment extends Fragment implements Callback<Base<ArrayList<Faq>>> {
    private Context mContext;
    private TextToSpeech mTts;
    private RecyclerView mFaqRecycler;
    private View mView;
    private FaqAdapter mFaqAdapter;
    private Button addFaq;
    private ArrayList<String> cats = new ArrayList<>();
    private MaterialDialog addFaqDialog;
    private Callback<me.bagus.foodcombining.api.admin.models.Base<ArrayList<FaqCategory>>> categoryCallback = new Callback<me.bagus.foodcombining.api.admin.models.Base<ArrayList<FaqCategory>>>() {
        @Override
        public void onResponse(Call<me.bagus.foodcombining.api.admin.models.Base<ArrayList<FaqCategory>>> call, Response<me.bagus.foodcombining.api.admin.models.Base<ArrayList<FaqCategory>>> response) {
            for (FaqCategory cat : response.body().data) {
                cats.add(cat.name);
            }
        }

        @Override
        public void onFailure(Call<me.bagus.foodcombining.api.admin.models.Base<ArrayList<FaqCategory>>> call, Throwable t) {

        }
    };

    public FaqFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FaqFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FaqFragment newInstance() {
        FaqFragment fragment = new FaqFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mTts = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_admin_faq, container, false);
        mFaqRecycler = (RecyclerView) mView.findViewById(R.id.faq_list);
        addFaq = (Button) mView.findViewById(R.id.add_faq);

        ApiService.getFaqServices().getFaqs().enqueue(this);
        AdminApiService.getFaqApiServices().getFaqCategories().enqueue(categoryCallback);
        addFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addFaqDialog != null && addFaqDialog.isShowing()) {
                    addFaqDialog.dismiss();
                    addFaqDialog = null;
                }

                addFaqDialog = new MaterialDialog.Builder(getContext())
                        .title("Tambah Faq")
                        .customView(R.layout.layout_dialog_add_faq, true)
                        .positiveText("Save")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {
                                View view = dialog.getCustomView();
                                Spinner spinner = (Spinner) view.findViewById(R.id.faq_cats);
                                EditText questionField = (EditText) view.findViewById(R.id.faq_question);
                                EditText answerField = (EditText) view.findViewById(R.id.faq_answer);
                                Log.d("faq cat", spinner.getSelectedItem().toString());
                                Log.d("faq question", questionField.getText().toString());
                                Log.d("faq answer", answerField.getText().toString());
                                AdminApiService
                                        .getFaqApiServices()
                                        .storeFaqs(questionField.getText().toString(),
                                                answerField.getText().toString(),
                                                spinner.getSelectedItem().toString())
                                        .enqueue(new Callback<AfterChangeCallback>() {
                                            @Override
                                            public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
                                                ApiService.getFaqServices().getFaqs().enqueue(FaqFragment.this);
                                                dialog.dismiss();
                                            }

                                            @Override
                                            public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
                                                t.printStackTrace();
                                                dialog.dismiss();
                                            }
                                        });
                            }
                        })
                        .build();

                Spinner spinner = (Spinner) addFaqDialog.findViewById(R.id.faq_cats);
                ArrayAdapter<CharSequence> adapter =
                        new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                adapter.addAll(cats);
                spinner.setAdapter(adapter);

                addFaqDialog.show();
            }
        });
        return mView;
    }

    @Override
    public void onResponse(Call<Base<ArrayList<Faq>>> call, Response<Base<ArrayList<Faq>>> response) {
        mFaqAdapter = new FaqAdapter(response.body().data, mTts);
        mFaqRecycler.setAdapter(mFaqAdapter);
        mFaqRecycler.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public void onFailure(Call<Base<ArrayList<Faq>>> call, Throwable t) {
        t.printStackTrace();
        Snackbar.make(mView, "failed to fetch faqs", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        if (mTts.isSpeaking()) {
            mTts.stop();
        }

        if (mTts != null) {
            mTts.shutdown();
            mTts = null;
        }

        super.onDestroy();
    }
}
