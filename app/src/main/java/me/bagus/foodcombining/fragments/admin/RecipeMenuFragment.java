package me.bagus.foodcombining.fragments.admin;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.chad.library.adapter.base.BaseQuickAdapter;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.RecipeListAdapter;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Recipe;
import me.bagus.foodcombining.interfaces.admin.FragmentAction;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeMenuFragment extends Fragment implements Callback<Base<ArrayList<Recipe>>> {
  private View mView;
  @BindView(R.id.fragment_recipe_list_recycler) RecyclerView mRecyclerView;
  private RecipeListAdapter mListAdapter;
  private FragmentAction mFragmentAction;

  public RecipeMenuFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mView = inflater.inflate(R.layout.fragment_recipe_menu, container, false);
    ButterKnife.bind(this, mView);

    mFragmentAction = (FragmentAction) getActivity();

    refreshData();

    return mView;
  }

  @OnClick(R.id.fragment_recipe_list_fab) public void onAddRecipeClicked(View view) {
    mFragmentAction.addRecipe();
  }

  public void refreshData() {
    AdminApiService.getRecipeApiServices().getRecipes().enqueue(this);
  }

  @Override public void onResponse(Call<Base<ArrayList<Recipe>>> call,
      Response<Base<ArrayList<Recipe>>> response) {
    mListAdapter = new RecipeListAdapter(response.body().data);

    mListAdapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            Recipe recipe = mListAdapter.getItem(i);

            Parcelable parcelable = Parcels.wrap(recipe);

            mFragmentAction.editRecipe(parcelable);
          }
        });

    mRecyclerView.setAdapter(mListAdapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<Recipe>>> call, Throwable t) {
    t.printStackTrace();
  }
}
