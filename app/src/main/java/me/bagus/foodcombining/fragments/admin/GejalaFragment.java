package me.bagus.foodcombining.fragments.admin;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.chad.library.adapter.base.BaseQuickAdapter;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.admin.GejalaListAdapter;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Gejala;
import me.bagus.foodcombining.interfaces.admin.FragmentAction;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GejalaFragment extends Fragment implements Callback<Base<ArrayList<Gejala>>> {
  private View mView;
  @BindView(R.id.fragment_gejala_recycler) RecyclerView mRecyclerView;
  private FragmentAction mFragmentAction;

  public GejalaFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_gejala, container, false);
    ButterKnife.bind(this, mView);
    mFragmentAction = (FragmentAction) getActivity();

    refreshData();

    return mView;
  }

  public void refreshData() {
    AdminApiService.getGejalaApiServices().getGejalas().enqueue(this);
  }

  @OnClick(R.id.fragment_gejala_fab) public void onAddGejala(View view) {
    mFragmentAction.addGejala();
  }

  @Override public void onResponse(Call<Base<ArrayList<Gejala>>> call,
      Response<Base<ArrayList<Gejala>>> response) {
    final GejalaListAdapter adapter = new GejalaListAdapter(response.body().data);
    adapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            mFragmentAction.editGejala(Parcels.wrap(adapter.getItem(i)));
          }
        });

    mRecyclerView.setAdapter(adapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<Gejala>>> call, Throwable t) {
    t.printStackTrace();
  }
}
