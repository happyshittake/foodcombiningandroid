package me.bagus.foodcombining.fragments.admin;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chad.library.adapter.base.BaseQuickAdapter;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.admin.KonsultasiListAdapter;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Consultation;
import me.bagus.foodcombining.interfaces.admin.FragmentAction;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class KonsultasiFragment extends Fragment
    implements Callback<Base<ArrayList<Consultation>>> {
  private View mView;
  @BindView(R.id.fragment_konsultasi_recycler) RecyclerView mRecyclerView;
  private FragmentAction mFragmentAction;

  public KonsultasiFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mView = inflater.inflate(R.layout.fragment_konsultasi2, container, false);
    ButterKnife.bind(this, mView);
    mFragmentAction = (FragmentAction) getActivity();

    refreshData();

    return mView;
  }

  public void refreshData() {
    AdminApiService.getKonsultasiApiServices().getConsults().enqueue(this);
  }

  @Override public void onResponse(Call<Base<ArrayList<Consultation>>> call,
      Response<Base<ArrayList<Consultation>>> response) {
    final KonsultasiListAdapter adapter = new KonsultasiListAdapter(response.body().data);
    adapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            mFragmentAction.answerConsultationQuestion(Parcels.wrap(adapter.getItem(i)));
          }
        });

    mRecyclerView.setAdapter(adapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<Consultation>>> call, Throwable t) {
    t.printStackTrace();
  }
}
