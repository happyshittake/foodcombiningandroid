package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.MenuRecipe;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.CategoryPickerByTime;
import me.bagus.foodcombining.utils.PrefUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddDiaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddDiaryFragment extends Fragment
    implements Callback<me.bagus.foodcombining.api.models.Response> {

  private View mView;
  private FragmentCallback mCallback;

  public AddDiaryFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment AddDiaryFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static AddDiaryFragment newInstance() {
    AddDiaryFragment fragment = new AddDiaryFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mView = inflater.inflate(R.layout.fragment_add_diary, container, false);
    final TimePicker timePicker = (TimePicker) mView.findViewById(R.id.diary_timepicker);
    final EditText editText = (EditText) mView.findViewById(R.id.diary_nama_makanan);
    final Switch switcher = (Switch) mView.findViewById(R.id.diary_switcher);
    final Spinner foodSpinner = (Spinner) mView.findViewById(R.id.diary_list_makanan);
    final Context context = getContext();
    mCallback = (FragmentCallback) context;
    Button button = (Button) mView.findViewById(R.id.save_diary);
    setUpFoodList(foodSpinner);

    switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
          editText.setEnabled(true);
          foodSpinner.setEnabled(false);
        } else {
          foodSpinner.setEnabled(true);
          editText.setEnabled(false);
        }
      }
    });

    switcher.setChecked(false);

    timePicker.setIs24HourView(true);

    button.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        String nama;
        if (switcher.isChecked()) {
          nama = editText.getText().toString();
        } else {
          nama = foodSpinner.getSelectedItem().toString();
        }
        String tipe = CategoryPickerByTime.pick(hour, minute);
        String timestamp = format.format(cal.getTime());

        ApiService.getDiaryServices()
            .storeDiary(nama, tipe, timestamp, PrefUtils.getCurrentUser(context).token)
            .enqueue(AddDiaryFragment.this);
      }
    });

    return mView;
  }

  private void setUpFoodList(final Spinner foodSpinner) {
    ApiService.getFoodMenuServices()
        .getFoodMenu(PrefUtils.getCurrentUser(getContext()).token)
        .enqueue(new Callback<Base<ArrayList<MenuRecipe>>>() {
          @Override public void onResponse(Call<Base<ArrayList<MenuRecipe>>> call,
              Response<Base<ArrayList<MenuRecipe>>> response) {
            ArrayAdapter<CharSequence> adapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            for (MenuRecipe recipe : response.body().data) {
              adapter.add(recipe.nama);
            }
            foodSpinner.setAdapter(adapter);
          }

          @Override public void onFailure(Call<Base<ArrayList<MenuRecipe>>> call, Throwable t) {
            t.printStackTrace();
          }
        });
  }

  @Override public void onResponse(Call<me.bagus.foodcombining.api.models.Response> call,
      Response<me.bagus.foodcombining.api.models.Response> response) {
    mCallback.onDiarySaved();
  }

  @Override
  public void onFailure(Call<me.bagus.foodcombining.api.models.Response> call, Throwable t) {
    t.printStackTrace();
  }
}
