package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.chad.library.adapter.base.BaseQuickAdapter;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.FoodMenuAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.MenuRecipe;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuFragment extends Fragment implements Callback<Base<ArrayList<MenuRecipe>>> {

  private RecyclerView mMenuList;
  private ArrayList<MenuRecipe> menuRecipes;
  private Button mAddMenuButton;
  private int mUserToken;

  public MenuFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment MenuFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static MenuFragment newInstance() {
    MenuFragment fragment = new MenuFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_menu, container, false);
    mMenuList = (RecyclerView) view.findViewById(R.id.menu_list);
    Button button = (Button) view.findViewById(R.id.add_new_menu);
    final FragmentCallback callback = (FragmentCallback) getActivity();
    User user = PrefUtils.getCurrentUser(getContext());
    mUserToken = user.token;

    ApiService.getFoodMenuServices().getFoodMenu(user.token).enqueue(this);

    button.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        callback.onAddMenuButtonClicked();
      }
    });
    return view;
  }

  public void refreshData() {
    ApiService.getFoodMenuServices().getFoodMenu(mUserToken).enqueue(this);
  }

  @Override public void onResponse(Call<Base<ArrayList<MenuRecipe>>> call,
      Response<Base<ArrayList<MenuRecipe>>> response) {
    menuRecipes = response.body().data;
    final FoodMenuAdapter adapter = new FoodMenuAdapter(response.body().data);
    adapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            FragmentCallback callback = (FragmentCallback) getContext();
            callback.onMenuItemClicked(adapter.getItem(i).id);
          }
        });
    mMenuList.setAdapter(adapter);
    mMenuList.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<MenuRecipe>>> call, Throwable t) {
    t.printStackTrace();
  }
}
