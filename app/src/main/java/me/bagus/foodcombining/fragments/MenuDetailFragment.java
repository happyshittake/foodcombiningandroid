package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.MenuCommentAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.MenuRecipeDetail;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MenuDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuDetailFragment extends Fragment
    implements View.OnClickListener, Callback<Base<MenuRecipeDetail>> {

  @BindView(R.id.menu_detail_kategori) TextView mTextKategori;
  @BindView(R.id.menu_detail_keterangan) TextView mTextKeterangan;
  private int mMenuId;
  private RecyclerView mCommentList;
  private MenuCommentAdapter mAdapter;
  private TextView mTextnama;
  private TextView mTextAuthor;
  private TextView mTextDeskripsi;
  private EditText mCommentField;
  private ImageView mImageView;
  private Button mAddButton;

  public MenuDetailFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment MenuDetailFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static MenuDetailFragment newInstance(int menuId) {
    MenuDetailFragment fragment = new MenuDetailFragment();
    Bundle args = new Bundle();
    args.putInt("menu_id", menuId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mMenuId = getArguments().getInt("menu_id");
      Log.d("DEBUG", "MENU ID: " + mMenuId);
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_menu_detail, container, false);
    ButterKnife.bind(this, view);
    mCommentList = (RecyclerView) view.findViewById(R.id.menu_detail_comment_list);
    mTextnama = (TextView) view.findViewById(R.id.menu_detail_nama);
    mTextDeskripsi = (TextView) view.findViewById(R.id.menu_detail_deskripsi);
    mTextAuthor = (TextView) view.findViewById(R.id.menu_detail_author);
    mCommentField = (EditText) view.findViewById(R.id.menu_detail_comment_field);
    mAddButton = (Button) view.findViewById(R.id.menu_detail_add_comment);
    mImageView = (ImageView) view.findViewById(R.id.menu_detail_image);

    refreshData();

    mAddButton.setOnClickListener(this);
    return view;
  }

  private void refreshData() {
    ApiService.getFoodMenuServices().getFoodMenuDetail(mMenuId).enqueue(this);
  }

  @Override public void onClick(View view) {
    String text = mCommentField.getText().toString();
    User user = PrefUtils.getCurrentUser(getContext());

    ApiService.getFoodMenuServices()
        .postComment(mMenuId, text, user.token)
        .enqueue(new Callback<me.bagus.foodcombining.api.models.Response>() {
          @Override public void onResponse(Call<me.bagus.foodcombining.api.models.Response> call,
              Response<me.bagus.foodcombining.api.models.Response> response) {
            mCommentField.setText(null);
            refreshData();
          }

          @Override public void onFailure(Call<me.bagus.foodcombining.api.models.Response> call,
              Throwable t) {
            t.printStackTrace();
          }
        });
  }

  @Override public void onResponse(Call<Base<MenuRecipeDetail>> call,
      Response<Base<MenuRecipeDetail>> response) {
    MenuRecipeDetail detail = response.body().data;
    Log.d("DEBUG", response.body().toString());
    if (mAdapter != null) {
      mAdapter.swap(detail.comments);
    } else {
      mTextnama.setText(detail.nama);
      mTextAuthor.setText(detail.author);
      mTextDeskripsi.setText(detail.deskripsi);
      mTextKategori.setText(detail.kategori);
      mTextKeterangan.setText(detail.keterangan);
      Glide.with(getContext())
          .load(detail.image)
          .crossFade()
          .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.no_image_placeholder))
          .centerCrop()
          .into(mImageView);

      mAdapter = new MenuCommentAdapter(detail.comments);
      mCommentList.setAdapter(mAdapter);
      mCommentList.setLayoutManager(new LinearLayoutManager(getContext()));
    }
  }

  @Override public void onFailure(Call<Base<MenuRecipeDetail>> call, Throwable t) {
    t.printStackTrace();
  }
}
