package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.KonsultasiAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.KonsultasiItem;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import me.bagus.foodcombining.utils.PrefUtils;
import me.bagus.foodcombining.utils.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link KonsultasiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KonsultasiFragment extends Fragment
    implements Callback<Base<ArrayList<KonsultasiItem>>> {
  private View mView;
  private RecyclerView mKonsultasiList;

  public KonsultasiFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment KonsultasiFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static KonsultasiFragment newInstance() {
    KonsultasiFragment fragment = new KonsultasiFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_konsultasi, container, false);
    mKonsultasiList = (RecyclerView) mView.findViewById(R.id.konsultasi_list);
    Button button = (Button) mView.findViewById(R.id.tambah_pertanyaan);
    final FragmentCallback callback = (FragmentCallback) getContext();

    User user = PrefUtils.getCurrentUser(getContext());
    ApiService.getKonsultasiServices().getUserQuestions(user.token).enqueue(this);

    button.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        callback.onTambahPertanyaanClicked();
      }
    });

    return mView;
  }

  @Override public void onResponse(Call<Base<ArrayList<KonsultasiItem>>> call,
      Response<Base<ArrayList<KonsultasiItem>>> response) {
    KonsultasiAdapter adapter = new KonsultasiAdapter(response.body().data);

    mKonsultasiList.setAdapter(adapter);
    mKonsultasiList.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<KonsultasiItem>>> call, Throwable t) {
    t.printStackTrace();
  }
}
