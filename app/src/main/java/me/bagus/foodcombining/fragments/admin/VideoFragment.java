package me.bagus.foodcombining.fragments.admin;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.admin.VideoAdapter;
import me.bagus.foodcombining.api.admin.AdminApi;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.AfterChangeCallback;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Video;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment implements Callback<Base<ArrayList<Video>>> {
  @BindView(R.id.video_recycler) RecyclerView videoRecycler;
  @BindView(R.id.video_add) FloatingActionButton videoAddButton;
  private View rootView;
  private AdminApi.VideoApi videoApi;
  private MaterialDialog addVideoDialog;
  private Callback<AfterChangeCallback> addVideoCallback = new Callback<AfterChangeCallback>() {
    @Override
    public void onResponse(Call<AfterChangeCallback> call, Response<AfterChangeCallback> response) {
      if (addVideoDialog != null) {
        addVideoDialog.dismiss();
      }
      videoApi.getVideos().enqueue(VideoFragment.this);
    }

    @Override public void onFailure(Call<AfterChangeCallback> call, Throwable t) {
      t.printStackTrace();
    }
  };

  public VideoFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    rootView = inflater.inflate(R.layout.fragment_video, container, false);
    ButterKnife.bind(this, rootView);
    videoApi = AdminApiService.getVideoApiServices();
    videoApi.getVideos().enqueue(this);
    return rootView;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override public void onResponse(Call<Base<ArrayList<Video>>> call,
      Response<Base<ArrayList<Video>>> response) {
    VideoAdapter adapter = new VideoAdapter(response.body().data);
    videoRecycler.setAdapter(adapter);
    videoRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<Video>>> call, Throwable t) {
    t.printStackTrace();
  }

  @OnClick(R.id.video_add) void onAddVideoButtonClick(View v) {
    addVideoDialog = new MaterialDialog.Builder(getContext()).input("id vide youtube", "", false,
        new MaterialDialog.InputCallback() {
          @Override public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
            Toast.makeText(getContext(), input.toString(), Toast.LENGTH_SHORT).show();
            videoApi.storeVideo(input.toString()).enqueue(addVideoCallback);
          }
        }).inputType(InputType.TYPE_CLASS_TEXT).content("Tambah Video baru").build();
    addVideoDialog.show();
  }
}
