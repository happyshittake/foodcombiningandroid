package me.bagus.foodcombining.fragments.admin;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.chad.library.adapter.base.BaseQuickAdapter;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.admin.AdminMemberListAdapter;
import me.bagus.foodcombining.api.admin.AdminApiService;
import me.bagus.foodcombining.api.admin.models.Base;
import me.bagus.foodcombining.api.admin.models.Member;
import me.bagus.foodcombining.interfaces.admin.FragmentAction;
import org.parceler.Parcels;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminMemberMenuFragment extends Fragment implements Callback<Base<ArrayList<Member>>> {
  private View mView;
  private AdminMemberListAdapter mMemberListAdapter;
  @BindView(R.id.fragment_member_list_recycler) RecyclerView mRecyclerView;
  private FragmentAction mFragmentAction;

  public AdminMemberMenuFragment() {
    // Required empty public constructor
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mFragmentAction = (FragmentAction) getActivity();
    mView = inflater.inflate(R.layout.fragment_admin_member_menu, container, false);
    ButterKnife.bind(this, mView);

    refreshData();

    return mView;
  }

  public void refreshData() {
    AdminApiService.getMemberApiServices().getMembers().enqueue(this);
  }

  @Override public void onResponse(Call<Base<ArrayList<Member>>> call,
      Response<Base<ArrayList<Member>>> response) {
    mMemberListAdapter = new AdminMemberListAdapter(response.body().data);
    mMemberListAdapter.setOnRecyclerViewItemClickListener(
        new BaseQuickAdapter.OnRecyclerViewItemClickListener() {
          @Override public void onItemClick(View view, int i) {
            Member member = mMemberListAdapter.getItem(i);
            Parcelable parcelable = Parcels.wrap(member);

            mFragmentAction.editMember(parcelable);
          }
        });

    mRecyclerView.setAdapter(mMemberListAdapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onFailure(Call<Base<ArrayList<Member>>> call, Throwable t) {
    t.printStackTrace();
  }
}
