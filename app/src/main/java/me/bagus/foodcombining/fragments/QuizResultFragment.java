package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import java.util.List;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.QuizAnswerHistory;
import me.bagus.foodcombining.adapters.QuizResultAdapter;
import org.parceler.Parcels;

public class QuizResultFragment extends Fragment {
  @BindView(R.id.fragment_quiz_result_recycler_view) RecyclerView mRecyclerView;
  private List<QuizAnswerHistory> mAnswerHistories;
  private Unbinder unbinder;

  public static QuizResultFragment newInstance(List<QuizAnswerHistory> answerHistories) {
    QuizResultFragment fragment = new QuizResultFragment();
    Bundle args = new Bundle();
    args.putParcelable("answer", Parcels.wrap(answerHistories));
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mAnswerHistories = Parcels.unwrap(getArguments().getParcelable("answer"));
    }
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_quiz_result, container, false);
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    unbinder = ButterKnife.bind(this, view);
    QuizResultAdapter adapter = new QuizResultAdapter(mAnswerHistories);
    mRecyclerView.setAdapter(adapter);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
