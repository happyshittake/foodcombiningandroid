package me.bagus.foodcombining.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.AnswerHistory;
import me.bagus.foodcombining.adapters.QuizScoreAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.QuizScore;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ndjoe on 17/01/17.
 */

public class ListGameScoreFragment extends Fragment implements Callback<Base<ArrayList<QuizScore>>> {
    @BindView(R.id.fragment_list_game_score_recylcer)
    RecyclerView listScore;
    @BindView(R.id.check_game_result_button)
    Button checkResultButton;
    private ArrayList<AnswerHistory> histories;
    private String result;

    public static ListGameScoreFragment newInstance(String result, ArrayList<AnswerHistory> historyArraylist) {
        ListGameScoreFragment fragment = new ListGameScoreFragment();
        Bundle args = new Bundle();
        args.putString("result", result);
        args.putParcelable("histories", Parcels.wrap(historyArraylist));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            result = getArguments().getString("result");
            histories = Parcels.unwrap(getArguments().getParcelable("histories"));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_game_score, container, false);
        ButterKnife.bind(this, view);

        ApiService.getSwipeGameServices().getAllScores().enqueue(this);
        return view;
    }

    @Override
    public void onResponse(Call<Base<ArrayList<QuizScore>>> call, Response<Base<ArrayList<QuizScore>>> response) {
        QuizScoreAdapter adapter = new QuizScoreAdapter(response.body().data);
        listScore.setAdapter(adapter);
        listScore.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onFailure(Call<Base<ArrayList<QuizScore>>> call, Throwable t) {
        t.printStackTrace();
    }

    @OnClick(R.id.check_game_result_button)
    public void showDetail(View v) {
        FragmentCallback callback = (FragmentCallback) getActivity();

        callback.onGameResultShow(result, histories);
    }
}
