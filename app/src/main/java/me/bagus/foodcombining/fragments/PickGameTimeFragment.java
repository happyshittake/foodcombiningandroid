package me.bagus.foodcombining.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.interfaces.FragmentCallback;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PickGameTimeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PickGameTimeFragment extends Fragment implements View.OnClickListener {

    private Context mContext;

    public PickGameTimeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PickGameTimeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PickGameTimeFragment newInstance() {
        PickGameTimeFragment fragment = new PickGameTimeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pick_game_time, container, false);
        Button pickPagi = (Button) view.findViewById(R.id.pick_pagi);
        Button pickSiang = (Button) view.findViewById(R.id.pick_siang);
        Button pickMalam = (Button) view.findViewById(R.id.pick_malam);

        mContext = getContext();

        for (Button button : Arrays.asList(pickPagi, pickMalam, pickSiang)) {
            button.setOnClickListener(this);
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        String text = ((Button) view).getText().toString();
        FragmentCallback callback = (FragmentCallback) mContext;

        switch (text) {
            case "SIANG":
                callback.onGameTimePicked("SIANG");
                break;
            case "MALAM":
                callback.onGameTimePicked("MALAM");
                break;
            default:
                callback.onGameTimePicked("PAGI");
                break;
        }
    }
}
