package me.bagus.foodcombining.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import it.sephiroth.android.library.tooltip.Tooltip;
import java.util.ArrayList;
import me.bagus.foodcombining.R;
import me.bagus.foodcombining.adapters.AnswerHistory;
import me.bagus.foodcombining.adapters.SwipeCardAdapter;
import me.bagus.foodcombining.api.ApiService;
import me.bagus.foodcombining.api.models.Base;
import me.bagus.foodcombining.api.models.SwipeCard;
import me.bagus.foodcombining.interfaces.FragmentCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SwipingGameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SwipingGameFragment extends Fragment implements Callback<Base<ArrayList<SwipeCard>>> {
  private static final String TIME = "time";

  private String mTime;
  private TextView mTextTimer;
  private Context mContext;
  private int mGameScore;
  private CountDownTimer mTimer;
  private SwipeFlingAdapterView mSwipeContainer;
  private View mView;
  private ArrayList<AnswerHistory> mAnswer = new ArrayList<>();

  public SwipingGameFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment SwipingGameFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static SwipingGameFragment newInstance(String time) {
    SwipingGameFragment fragment = new SwipingGameFragment();
    Bundle args = new Bundle();
    args.putString(TIME, time);
    fragment.setArguments(args);
    return fragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mGameScore = 0;
    if (getArguments() != null) {
      mTime = getArguments().getString(TIME);
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    mView = inflater.inflate(R.layout.fragment_swiping_game, container, false);
    mTextTimer = (TextView) mView.findViewById(R.id.game_timer);
    mContext = getContext();
    mSwipeContainer = (SwipeFlingAdapterView) mView.findViewById(R.id.swiping_view);

    ApiService.getSwipeGameServices()
        .getCards(mTime.toLowerCase())
        .enqueue(SwipingGameFragment.this);
    mTextTimer.setText("0");

    return mView;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Tooltip.make(getContext(), new Tooltip.Builder(101).anchor(mTextTimer, Tooltip.Gravity.TOP)
        .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false),
            3000)
        .activateDelay(800)
        .showDelay(300)
        .text(
            "Swipe kekiri jika foto tidak cocok dengan waktu yang dipilih, swipe kekanan jika cocok")
        .maxWidth(500)
        .withArrow(false)
        .withOverlay(true)
        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
        .build()).show();
  }

  @Override public void onResponse(final Call<Base<ArrayList<SwipeCard>>> call,
      Response<Base<ArrayList<SwipeCard>>> response) {
    final ArrayList<SwipeCard> swipeCards = response.body().data;
    final FragmentCallback callback = (FragmentCallback) mContext;

    final SwipeCardAdapter adapter = new SwipeCardAdapter(mContext, swipeCards);
    mSwipeContainer.setAdapter(adapter);
    mSwipeContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
      @Override public void removeFirstObjectInAdapter() {
        Log.d("LIST", "removed ogetSwipeTo()bject!");

        if (swipeCards.size() == 0) {
          //callback.(gameScore);
          callback.onSwipeGameEnded(mGameScore, mAnswer);
        } else {
          swipeCards.remove(0);
          adapter.notifyDataSetChanged();
        }
      }

      @Override public void onLeftCardExit(Object o) {
        if (swipeCards.size() > 0) {
          SwipeCard card = swipeCards.get(0);
          mAnswer.add(new AnswerHistory(card.answer, "left"));
          if (card.answer.equalsIgnoreCase("left")) {
            Log.i("BENAR_SALAH", "BENAR");
            mGameScore += 1;
          }
        } else {
          //handler.onGameEnded(gameScore);
          callback.onSwipeGameEnded(mGameScore, mAnswer);
        }
      }

      @Override public void onRightCardExit(Object o) {
        if (swipeCards.size() > 0) {
          SwipeCard card = swipeCards.get(0);
          mAnswer.add(new AnswerHistory(card.answer, "right"));
          if (card.answer.equalsIgnoreCase("right")) {
            Log.i("BENAR_SALAH", "BENAR");
            mGameScore += 1;
          }
        } else {
          //handler.onGameEnded(gameScore);
          callback.onSwipeGameEnded(mGameScore, mAnswer);
        }
      }

      @Override public void onAdapterAboutToEmpty(int i) {

      }

      @Override public void onScroll(float v) {

      }
    });

    adapter.notifyDataSetChanged();
    mTimer = new CountDownTimer(60000, 1000) {
      @Override public void onTick(long l) {
        mTextTimer.setText(String.format("%d", l / 1000));
      }

      @Override public void onFinish() {
        callback.onSwipeGameEnded(mGameScore, mAnswer);
      }
    };

    mTimer.start();
  }

  @Override public void onFailure(Call<Base<ArrayList<SwipeCard>>> call, Throwable t) {
    Snackbar.make(mView, "failed to fetch cards", Snackbar.LENGTH_SHORT).show();
  }

  @Override public void onDestroy() {
    if (mTimer != null) {
      mTimer.cancel();
    }

    super.onDestroy();
  }
}
